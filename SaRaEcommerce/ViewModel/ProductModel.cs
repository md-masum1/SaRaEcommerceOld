﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaRaEcommerce.ViewModel
{
    public class ProductListPage
    {
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int? SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public int? SubSubCategoryId { get; set; }
        public string SubSubCategoryName { get; set; }

        public string SizeId { get; set; }
        public string ColorId { get; set; }
        public string[] SizeArray { get; set; }
        public string[] ColorArray { get; set; }
        public double? MinPrice { get; set; }
        public double? MaxPrice { get; set; }

        public string PageLength { get; set; }
        public string ShortBy { get; set; }

        public int? CurrentPage { get; set; }
        public int? TotalProduct { get; set; }

        public int? Take { get; set; }
        public int? Skip { get; set; }

        public IEnumerable<ProductModel> ProductModels { get; set; }
    }

    public class ProductListSideBar
    {
        public int? CategoryId { get; set; }
        public int? SubCategoryId { get; set; }
        public int? SubSubCategoryId { get; set; }

        public double? MinPrice { get; set; }
        public double? MaxPrice { get; set; }

        public IEnumerable<MenuMain> MenuMains { get; set; }
        public IEnumerable<ColorForFilter> Colors { get; set; }
        public IEnumerable<SizeForFilter> Sizes { get; set; }
        public IEnumerable<ProductModel> ProductModels { get; set; }
    }

    public class ColorForFilter
    {
        public int ColorId { get; set; }
        public string ColorName { get; set; }
        public bool Checked { get; set; }
    }

    public class SizeForFilter
    {
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public bool Checked { get; set; }
    }

    public class SingleProduct
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Sku { get; set; }
        public string FabricName { get; set; }

        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }

        public int? SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }

        public int? SubSubCategoryId { get; set; }
        public string SubSubCategoryName { get; set; }

        public string ShortDescription { get; set; }
        [AllowHtml]
        public string LongDescription { get; set; }

        public string PrimaryImage { get; set; }
        public string InsertDate { get; set; }
        public double SellAmount { get; set; }
        public double Discount { get; set; }
        public double NewAmount { get; set; }
        public double TotalQuantity { get; set; }

        public double ReviewScore { get; set; }
        public int TotalReviewCount { get; set; }

        public IEnumerable<ProductModel> ProductModels { get; set; }
        public CustomerReview CustomerReview { get; set; }
        public IEnumerable<RatioList> RatioLists { get; set; }
    }

    public class RatioList
    {
        public int RatioId { get; set; }
        public string RatioName { get; set; }

        public IEnumerable<RatioSizeList> RatioSizeLists { get; set; }
    }

    public class RatioSizeList
    {
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public float SizeValue { get; set; }
    }

    public class ProductForQuickView
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public string Sku { get; set; }

        public string ShortDescription { get; set; }

        public string PrimaryImage { get; set; }

        public string InsertDate { get; set; }

        public double SellAmount { get; set; }
        public double Discount { get; set; }
        public double NewAmount { get; set; }

        public double TotalQuantity { get; set; }

        //public IEnumerable<RatioList> RatioLists { get; set; }
    }

    public class ProductModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public string Sku { get; set; }

        public string PrimaryImage { get; set; }

        public string InsertDate { get; set; }

        public double SellAmount { get; set; }
        public int Discount { get; set; }
        public double NewAmount { get; set; }

        public double TotalQuantity { get; set; }
    }

    public class ProductColor
    {
        public int ProductId { get; set; }

        public int ProductColorId { get; set; }
        public string ProductColorName { get; set; }
        public string ProductColorCode { get; set; }

        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
    }

    public class ProductSize
    {
        public int ProductId { get; set; }

        public int SizeId { get; set; }
        public string SizeName { get; set; }

        public double Quantity { get; set; }
    }

    public class CustomerReview
    {
        public int ReviewId { get; set; }

        public int ProductId { get; set; }

        public string CustomerId { get; set; }

        [Required]
        [DisplayName("Name")]
        public string CustomerName { get; set; }

        [Required]
        [DisplayName("Email")]
        [EmailAddress]
        public string CustomerEmail { get; set; }

        [DisplayName("Phone")]
        [StringLength(14, MinimumLength = 11)]
        public string CustomerPhone { get; set; }

        [Required]
        [AllowHtml]
        [DisplayName("Review")]
        [StringLength(2000, MinimumLength = 5)]
        public string Message { get; set; }

        public double Rating { get; set; }

        public string ReviewDate { get; set; }
    }
}