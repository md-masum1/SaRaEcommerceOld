﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaRaEcommerce.ViewModel
{
    public class CheckoutModel
    {
        public string CustomerId { get; set; }
        [Required]
        [DisplayName("Full Name:")]
        public string FirstName { get; set; }

        [DisplayName("Last Name:")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Invalid email format.")]
        [EmailAddress]
        [DisplayName("Email:")]
        public string Email { get; set; }

        [Required]
        [StringLength(14, MinimumLength = 11)]
        [DisplayName("Phone Number:")]
        public string Phone { get; set; }

        [Required]
        [DisplayName("Address:")]
        public string Address1 { get; set; }

        [DisplayName("Address 2:")]
        public string Address2 { get; set; }

        [DisplayName("City:")]
        public string City { get; set; }

        [Required]
        [DisplayName("Country:")]
        public string Country { get; set; }

        public int ShippingCost { get; set; }

        public string OrderNumber { get; set; }
        public string OrderDate { get; set; }

        public string CardType { get; set; }
        public string PaymentStatus { get; set; }

        public string CustomerIp { get; set; }

        public string TransId { get; set; }

        public string TotalAmount { get; set; }

        public string Discount { get; set; }
        public double DiscountAmount { get; set; }

        public string DiscountType { get; set; }

        public string TransSuccessStatus { get; set; }

        public IEnumerable<CartProduct> CartProducts { get; set; }
        public CartProduct CartProduct { get; set; }

        public ExecutePaymentResponse BkashPaymentResponse { get; set; }
    }

    public class CartProduct
    {
        public string OrderNumber { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string Sku { get; set; }

        public int ColorId { get; set; }
        public string ColorName { get; set; }

        public int SizeId { get; set; }
        public string SizeName { get; set; }

        public int Quantity { get; set; }

        public double Price { get; set; }
    }

    public class EmployeeModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Department { get; set; }

        public string Contact { get; set; }

        public string Email { get; set; }
    }

    public class TokenModel
    {
        public string token_type { get; set; }

        public string id_token { get; set; }

        public string expires_in { get; set; }

        public string refresh_token { get; set; }

        public string appKey { get; set; }
    }

    public class CreatePayment
    {
        public string paymentID { get; set; }

        public string createTime { get; set; }

        public string orgName { get; set; }

        public string orgLogo { get; set; }

        public string transactionStatus { get; set; }

        public string amount { get; set; }

        public string currency { get; set; }

        public string intent { get; set; }

        public string merchantInvoiceNumber { get; set; }
    }

    public class RequestPayment
    {
        public string Authorization { get; set; }

        public string appKey { get; set; }

        public string amount { get; set; }

        public string currency { get; set; }

        public string intent { get; set; }

        public string merchantInvoiceNumber { get; set; }
    }

    public class ExecutePayment
    {
        public string Authorization { get; set; }

        public string AppKey { get; set; }

        public string paymentID { get; set; }
    }

    public class ExecutePaymentResponse
    {
        public string OrderNumber { get; set; }

        public string paymentID { get; set; }

        public string createTime { get; set; }

        public string updateTime { get; set; }

        public string trxID { get; set; }

        public string transactionStatus { get; set; }

        public string Amount { get; set; }

        public string currency { get; set; }

        public string intent { get; set; }

        public string mercentInvoiceNumber { get; set; }

        public string errorCode { get; set; }

        public string errorMessage { get; set; }
    }

    public class QueryPayment
    {
        public string paymentID { get; set; }

        public string id_token { get; set; }

        public string createTime { get; set; }

        public string updateTime { get; set; }

        public string trxID { get; set; }

        public string transactionStatus { get; set; }

        public string amount { get; set; }

        public string currency { get; set; }

        public string intent { get; set; }

        public string merchantInvoiceNumber { get; set; }

        public string refundAmount { get; set; }
    }

    public class TransactionDetails
    {
        public string id_token { get; set; }

        public string amount { get; set; }

        public string completedTime { get; set; }

        public string currency { get; set; }

        public string customerMsisdn { get; set; }

        public string initiationTime { get; set; }

        public string organizationShortCode { get; set; }

        public string transactionReference { get; set; }

        public string transactionStatus { get; set; }

        public string transactionType { get; set; }

        public string trxID { get; set; }
    }
}