﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaRaEcommerce.ViewModel
{
    public class SmsGateway
    {
        public string MessageId { get; set; }

        public string Status { get; set; }

        public string StatusText { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorText { get; set; }

        public string SMSCount { get; set; }

        public string CurrentCredit { get; set; }
    }
}