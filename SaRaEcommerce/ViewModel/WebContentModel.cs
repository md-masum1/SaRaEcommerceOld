﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaRaEcommerce.ViewModel
{
    public class WebContentModel
    {
    }

    public class ReturnFormModel
    {
        public string OrderNumber { get; set; }

        [Required]
        [DisplayName("Reason for Return")]
        public string ReturnReason { get; set; }

        [Required]
        [DisplayName("Exchange For")]
        public string ExchangeType { get; set; }

        [Required]
        [DisplayName("Customer Comment's")]
        public string Comment { get; set; }

        public List<OrderedProductModel> OrderedProductModels { get; set; }
    }

    public class OrderedProductModel
    {
        [Required]
        public string OrderNumber { get; set; }

        [Required]
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string Sku { get; set; }

        [Required]
        public int ColorId { get; set; }

        public string ColorName { get; set; }

        [Required]
        public int SizeId { get; set; }

        public string SizeName { get; set; }

        [Required]
        public int Quantity { get; set; }

        public double Price { get; set; }
    }

    public class CareerModel
    {
        [Required]
        [DisplayName("Career Type")]
        public string CareerType { get; set; }

        [Required]
        public HttpPostedFileBase Cv { get; set; }
    }
}