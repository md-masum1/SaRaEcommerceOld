﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SaRaEcommerce.ViewModel
{
    public class HomeModel
    {
        public IEnumerable<HomepageSlider> HomepageSliders { get; set; }
        public IEnumerable<HomepageBanner> HomepageBanners { get; set; }
        public IEnumerable<HomepageOffer> HomepageOffers { get; set; }
    }

    public class HomepageSlider
    {
        public int SliderId { get; set; }

        public string SliderTitle { get; set; }

        public string SliderDescription { get; set; }

        public string SliderUrl { get; set; }

        public string SliderImage { get; set; }
    }

    public class HomepageOffer
    {
        public int OfferId { get; set; }

        public string OfferUrl { get; set; }

        public string OfferImage { get; set; }
    }

    public class HomepageBanner
    {
        public int BannerId { get; set; }

        public string BannerTitle { get; set; }

        public string BannerDescription { get; set; }

        public string BannerUrl { get; set; }

        public string BannerImage { get; set; }
    }

    public class MenuMain
    {
        public int MenuMainId { get; set; }

        public string MenuMainName { get; set; }

        public IEnumerable<MenuSub> MenuSubs { get; set; }
    }

    public class MenuSub
    {
        public int MenuMainId { get; set; }

        public int MenuSubId { get; set; }

        public string MenuSubName { get; set; }

        public IEnumerable<MenuSubSub> MenuSubSubs { get; set; }
    }

    public class MenuSubSub
    {
        public int MenuMainId { get; set; }

        public int MenuSubId { get; set; }

        public int MenuSubSubId { get; set; }

        public string MenuSubSubName { get; set; }
    }

    public class ProductDisplaySection
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string Sku { get; set; }

        public string ProductImage { get; set; }

        public string DiscountPercentage { get; set; }
        public string PreviousPrice { get; set; }
        public string NewPrice { get; set; }
    }

    public class CustomerContact
    {
        [Required]
        [StringLength(30, MinimumLength = 4)]
        [DisplayName("Name")]
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string CustomerEmail { get; set; }

        [Required]
        [StringLength(2000, MinimumLength = 30)]
        [Display(Name = "Message")]
        public string Message { get; set; }

        public string IpOrMac { get; set; }
    }
}