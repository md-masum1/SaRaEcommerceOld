﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaRaEcommerce.ViewModel
{
    public class CustomerModel
    {
        public string CustomerId { get; set; }

        [DisplayName("First Name:")]
        public string FirstName { get; set; }

        [DisplayName("Last Name:")]
        public string LastName { get; set; }

        [EmailAddress]
        [DisplayName("Email:")]
        public string Email { get; set; }

        [DisplayName("Phone Number:")]
        [StringLength(14, MinimumLength = 11)]
        public string Phone { get; set; }

        [DisplayName("Address 1:")]
        public string Address1 { get; set; }

        [DisplayName("Address 2:")]
        public string Address2 { get; set; }

        [DisplayName("Gender:")]
        public string Gender { get; set; }

        [DisplayName("City:")]
        public string City { get; set; }

        [DisplayName("Country:")]
        public string Country { get; set; }

        public IEnumerable<OrderStatus> OrderStatuses { get; set; }
    }

    public class OrderStatus
    {
        public string OrderNumber { get; set; }
        public string OrderDate { get; set; }

        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerAddress { get; set; }

        public double ShippingCost { get; set; }
        public string DiscountPercentage { get; set; }

        public string DeliveryStatus { get; set; }
        public string DeliveryDate { get; set; }

        public string ReturnStatus { get; set; }
        public string ReturnDate { get; set; }

        public string RejectStatus { get; set; }
        public string RejectDate { get; set; }
        public string RejectRemarks { get; set; }

        public string PaymentType { get; set; }
        public string PaymentStatus { get; set; }

        public IEnumerable<OrderSummery> OrderSummeries { get; set; }
    }

    public class OrderSummery
    {
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
        public string Sku { get; set; }
        public string ColorName { get; set; }
        public string SizeName { get; set; }

        public int Quantity { get; set; }
        public double Price { get; set; }
        public double TotalPrice { get; set; }
    }
}