﻿using System.Web;
using System.Web.Optimization;

namespace SaRaEcommerce
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/scriptGlobal").Include(
                "~/Scripts/jquery-migrate.min.js",
                "~/plugins/bootstrap/js/bootstrap.min.js",
                "~/plugins/layout/scripts/back-to-top.js",
                "~/plugins/jquery-slimscroll/jquery.slimscroll.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/scriptPage").Include(
                "~/plugins/fancybox/source/jquery.fancybox.pack.js",                //popup
                "~/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js",       //slider for products
                "~/plugins/zoom/jquery.zoom.min.js",                     //product zoom
                "~/plugins/bootstrap-touchspin/bootstrap.touchspin.js",                 //Quantity
                "~/plugins/slider-layer-slider/js/greensock.js",                          // External libraries: GreenSock
                "~/plugins/slider-layer-slider/js/layerslider.transitions.js",            // LayerSlider script files
                "~/plugins/slider-layer-slider/js/layerslider.kreaturamedia.jquery.js",   // LayerSlider script files
                "~/Content/bootstrap-toastr/toastr.min.js",
                "~/Scripts/jquery-confirm.js",
                "~/Scripts/ui-toastr.js",
                "~/plugins/pages/scripts/layerslider-init.js",
                "~/plugins/layout/scripts/layout.js",
                "~/plugins/pages/scripts/checkout.js",
                "~/Scripts/custom.js"));               

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/plugins/font-awesome/css/font-awesome.min.css",
                "~/plugins/bootstrap/css/bootstrap.min.css",
                "~/plugins/fancybox/source/jquery.fancybox.css",
                "~/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css",
                "~/plugins/slider-layer-slider/css/layerslider.css",
                "~/Content/bootstrap-toastr/toastr.min.css",
                "~/Content/jquery-confirm.css",
                "~/Content/components.css",
                "~/plugins/layout/css/style.css",
                "~/plugins/pages/css/style-shop.css",
                "~/plugins/pages/css/style-layer-slider.css",
                "~/plugins/layout/css/style-responsive.css"));
        }
    }
}
