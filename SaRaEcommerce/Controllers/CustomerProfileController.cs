﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using SaRaEcommerce.Dal;
using SaRaEcommerce.Models;
using SaRaEcommerce.ViewModel;

namespace SaRaEcommerce.Controllers
{
    [Authorize]
    public class CustomerProfileController : Controller
    {
        private ApplicationUserManager _userManager;
        private readonly CustomerDal _objCustomerDal = new CustomerDal();

        public CustomerProfileController()
        {
        }

        public CustomerProfileController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get => _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => _userManager = value;
        }

        public async Task<ActionResult> Dashboard()
        {
            CustomerModel model = new CustomerModel();
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

            if (user != null)
            {
                model.Email = user.Email;
                model.FirstName = user.FirstName;
                model.LastName = user.LastName;
                model.Phone = user.PhoneNo;
                model.Address1 = user.Address1;
                model.Address2 = user.Address2;
                model.City = user.City;
                model.Country = user.Country;
                model.Gender = user.Gender;
                model.CustomerId = user.Id; 
            }

            model.OrderStatuses = await _objCustomerDal.GetCustomerOrderStatus(model.CustomerId);

            return View(model);
        }

        public async Task<ActionResult> UpdateUser(CustomerModel objCustomerModel)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

                user.FirstName = objCustomerModel.FirstName;
                user.LastName = objCustomerModel.LastName;
                user.Email = objCustomerModel.Email;
                user.PhoneNo = objCustomerModel.Phone;
                user.Address1 = objCustomerModel.Address1;
                user.Address2 = objCustomerModel.Address2;
                user.City = objCustomerModel.City;
                user.Country = objCustomerModel.Country;
                user.Gender = objCustomerModel.Gender;

                await UserManager.UpdateAsync(user);
            }
            

            return RedirectToAction("Dashboard", "CustomerProfile");
        }

        public async Task<ActionResult> DeleteCustomerOrder(string orderNumber, string customerId)
        {
            string message = "";

            if (!string.IsNullOrWhiteSpace(orderNumber) && !string.IsNullOrWhiteSpace(customerId))
            {
                message = await _objCustomerDal.OrderedProductDelete(orderNumber, customerId);
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetCustomerOrderSummery(string orderNumber, string customerId)
        {
            OrderStatus objOrderStatus = new OrderStatus();

            if (!string.IsNullOrWhiteSpace(orderNumber) && !string.IsNullOrWhiteSpace(customerId))
            {
                objOrderStatus = await _objCustomerDal.GetCustomerSingleOrder(customerId, orderNumber);

                if (objOrderStatus != null)
                {
                    objOrderStatus.OrderSummeries = await _objCustomerDal.GetCustomerOrderSummery(customerId, orderNumber);
                }
            }
            return PartialView("_OrderSummeryPartial", objOrderStatus);
        }

        public ActionResult OrderTracking()
        {
            return View();
        }
    }
}