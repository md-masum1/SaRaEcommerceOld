﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SaRaEcommerce.Dal;
using SaRaEcommerce.Utility;
using SaRaEcommerce.ViewModel;

namespace SaRaEcomAdmin.Controllers
{
    public class WebContentController : Controller
    {
        private readonly WebContentDal _webContentDal = new WebContentDal();

        public ActionResult Faq()
        {
            return View();
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }

        public ActionResult TermsAndConditions()
        {
            return View();
        }

        public ActionResult CookiePolicy()
        {
            return View();
        }

        public ActionResult ReturnPolicy()
        {
            return View();
        }

        public ActionResult Cancellation()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        public ActionResult HowToOrder()
        {
            return View();
        }

        public ActionResult BillingAndPayments()
        {
            return View();
        }

        public ActionResult ShippingAndDelivery()
        {
            return View();
        }

        public ActionResult TrackYourOrders()
        {
            return View();
        }

        public ActionResult ReturnAndExchanges()
        {
            return View();
        }

        public ActionResult AboutSaRa()
        {
            return View();
        }

        public ActionResult OurPeople()
        {
            return View();
        }

        public ActionResult OurValues()
        {
            return View();
        }

        public ActionResult NewsAndEvents()
        {
            return View();
        }

        public ActionResult PhotoGallery()
        {
            return View();
        }

        public ActionResult VideoGallery()
        {
            return View();
        }
		public ActionResult SizeChart()
        {
            return View();
        }

        public ActionResult Sitemap()
        {
            return View();
        }

        public async Task<ActionResult> Career()
        {
            List<SelectListItem> objSelectListItems = new List<SelectListItem>
            {
                new SelectListItem() {Value = "", Text = @"Please select one"}
            };

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(UtilityClass.ErpHostAddress);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("Department")).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();

                    var result = JsonConvert.DeserializeObject<IList<Department>>(jsonstring.Result);

                    foreach (var o in result)
                    {
                        var id = Convert.ToDouble(o.DEPARTMENT_ID);
                        string name = o.DEPARTMENT_NAME.Trim();
                        objSelectListItems.Add(new SelectListItem() { Value = name, Text = name });
                    }
                }
            }

            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(UtilityClass.ErpHostAddress);
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    using (var response = await client.GetAsync("Department"))
            //    {
            //        if (response.IsSuccessStatusCode)
            //        {
            //            var result = await response.Content.ReadAsAsync<List<Department>>();

            //            foreach (var o in result)
            //            {
            //                var id = Convert.ToDouble(o.DEPARTMENT_ID);
            //                string name = o.DEPARTMENT_NAME.Trim();

            //                objSelectListItems.Add(new SelectListItem(){Value = name, Text = name});
            //            }

            //        };

            //    }
            //}

            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(UtilityClass.ErpHostAddress);

            //    var responseTask = client.GetAsync("Department");
            //    responseTask.Wait();

            //    //var result = responseTask.Result;
            //    if (responseTask.IsSuccessStatusCode)
            //    {
            //        var readTask = result.Content.ReadAsAsync<IList<Department>>();
            //        readTask.Wait();

            //        var product = readTask.Result;

            //        foreach (var o in product)
            //        {
            //            var id = Convert.ToDouble(o.DEPARTMENT_ID);
            //            string name = o.DEPARTMENT_NAME.Trim();

            //            objSelectListItems.Add(new SelectListItem(){Value = name, Text = name});
            //        }
            //    }
            //}

            ViewBag.DepartmentList = new SelectList(objSelectListItems, "Value", "Text");

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"] as string;
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Career(CareerModel objCareerModel)
        {
            if (!ModelState.IsValid)
            {
                TempData["message"] = "all field required";
                return RedirectToAction("Career", "WebContent");
            }

            string mailSubject = "SaRa LifeStyle Career For (" + objCareerModel.CareerType + ")";
            string mailBody = "Career mail request for (" + objCareerModel.CareerType + " ), Time: (" + DateTime.Now.ToString("f") + "), resume is attached";
            byte[] attachedFile = UtilityClass.FileToByteArray(objCareerModel.Cv);

            if (attachedFile != null && attachedFile.Length > 0)
            {
                new Thread(() =>
                {
                    UtilityClass.SendEmailCareer("erp@snowtex.org", mailSubject, mailBody, attachedFile);

                }).Start();

                new Thread(() =>
                {
                    UtilityClass.SendEmailCareer("recruitment@snowtex.org", mailSubject, mailBody, attachedFile);

                }).Start();

                TempData["message"] = "submit successfully done, we will verify your resume shortly";
                return RedirectToAction("Career", "WebContent");
            }
            else
            {
                TempData["message"] = "File must be pdf in format";
                return RedirectToAction("Career", "WebContent");
            }
        }

        #region Return Form Section

        public ActionResult ReturnForm()
        {
            ReturnFormModel model = new ReturnFormModel();

            if (TempData.ContainsKey("returnForm"))
            {
                model = TempData["returnForm"] as ReturnFormModel;
            }

            if (TempData.ContainsKey("Message"))
            {
                ViewBag.Message = TempData["Message"] as string;
            }
            ViewBag.ReturnReasonList = UtilityClass.ReturnReasonItem();
            ViewBag.ExchangeTypeList = UtilityClass.ExchangeTypeItem();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> ReturnForm(ReturnFormModel returnForm)
        {
            string returnMessage = "";
            if (!ModelState.IsValid)
            {
                var data1 = new
                {
                    message = "all field required",
                    redirectUrl = Url.Action("ReturnForm", "WebContent")
                };
                return Json(data1, JsonRequestBehavior.AllowGet);
            }

            if (returnForm.OrderedProductModels.Any())
            {
                var strMessage = await _webContentDal.SaveReturnRequest(returnForm);

                if (strMessage.Item2 != "0" && !string.IsNullOrWhiteSpace(strMessage.Item2))
                {
                    returnMessage =
                        await _webContentDal.SaveReturnRequestProduct(returnForm.OrderedProductModels, strMessage.Item2);
                }
                else
                {
                    returnMessage = strMessage.Item1;
                }
            }
            var data = new
            {
                message = returnMessage,
                redirectUrl = Url.Action("Index", "Home")
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetOrderProduct(string orderNumber)
        {
            var model = new List<OrderedProductModel>();

            if (!string.IsNullOrWhiteSpace(orderNumber))
            {
                model = await _webContentDal.GetOrderedProduct(orderNumber);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}