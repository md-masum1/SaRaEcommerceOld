﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaRaEcommerce.Dal;
using SaRaEcommerce.Models;
using SaRaEcommerce.Utility;
using SaRaEcommerce.ViewModel;

namespace SaRaEcommerce.Controllers
{
    [NoCache]
    public class ProductController : Controller
    {
        private readonly ProductModel _objProductModel = new ProductModel();
        private readonly ProductDal _objProductDal = new ProductDal();
        private readonly HomeDal _objHomeDal = new HomeDal();

        [HttpGet]
        public async Task<ActionResult> ProductList(int? categoryId, int? subCategoryId, int? subSubCategoryId)
        {
            ModelState.Clear();

            ProductListPage productList = new ProductListPage();

            ViewBag.PageLengthList = UtilityClass.ProductPageLengthItem();
            ViewBag.ShortByList = UtilityClass.ProductPageShortItem();

            productList.SizeArray = new string[]{};
            productList.ColorArray = new string[]{};

            productList.PageLength = "48";
            productList.CurrentPage = 1;
            productList.Take = 1;
            productList.Skip = 48;

            productList.ShortBy = "0";
            productList.TotalProduct = await _objProductDal.GetTotalProductCount(categoryId, subCategoryId, subSubCategoryId);

            productList.ProductModels = await _objProductDal.GetProductList(categoryId, subCategoryId, subSubCategoryId, productList.PageLength, productList.ShortBy, productList.CurrentPage, productList.Take, productList.Skip, null, null, null);

            if (categoryId != null && categoryId != 0)
            {
                productList.CategoryId = categoryId;
                productList.CategoryName = await _objProductDal.GetCategoryName((int)categoryId);
            }
            if (categoryId != null && categoryId != 0 && subCategoryId != null && subCategoryId != 0)
            {
                productList.SubCategoryId = subCategoryId;
                productList.SubCategoryName = await _objProductDal.GetSubCategoryName((int)categoryId, (int)subCategoryId);
            }
            if (categoryId != null && categoryId != 0 && subCategoryId != null && subCategoryId != 0 && subSubCategoryId != null && subSubCategoryId != 0)
            {
                productList.SubSubCategoryId = subSubCategoryId;
                productList.SubSubCategoryName = await _objProductDal.GetSubSubCategoryName((int)categoryId, (int)subCategoryId, (int)subSubCategoryId);
            }

            productList.ProductModels = productList.ProductModels.GroupBy(x => x.ProductId).Select(y => y.FirstOrDefault());

            return View(productList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ProductList(int? categoryId, int? subCategoryId, int? subSubCategoryId, string pageLength, string shortBy, int? currentPage, int? totalProduct, int? take, int? skip, string sizeId, string colorId, double? minPrice, double? maxPrice)
        {
            ModelState.Clear();

            ProductListPage productList = new ProductListPage();

            ViewBag.PageLengthList = UtilityClass.ProductPageLengthItem();
            ViewBag.ShortByList = UtilityClass.ProductPageShortItem();

            productList.SizeArray = sizeId.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            productList.ColorArray = colorId.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            productList.MinPrice = minPrice;
            productList.MaxPrice = maxPrice;

            productList.PageLength = pageLength ?? "48";
            productList.CurrentPage = currentPage ?? 1;
            productList.Take = take ?? 1;
            if(pageLength != null && skip == null)
                productList.Skip = Convert.ToInt32(pageLength);

            productList.Skip = skip;

            productList.ShortBy = shortBy;
            

            if (!string.IsNullOrWhiteSpace(sizeId) || !string.IsNullOrWhiteSpace(colorId))
            {
                IEnumerable<int> productIdList = new List<int>();

                if (!string.IsNullOrWhiteSpace(colorId))
                {
                    productIdList = await _objProductDal.GetProductIdByColor(categoryId, subCategoryId, subSubCategoryId, productList.ColorArray);
                }

                if (!string.IsNullOrWhiteSpace(sizeId))
                {
                    productIdList = await _objProductDal.GetProductIdBySize(categoryId, subCategoryId, subSubCategoryId, productList.SizeArray, productIdList.ToArray());
                }

                var idList = productIdList.ToList();
                productList.TotalProduct = idList.Count;
                if (idList.Any())
                {
                    productList.ProductModels = await _objProductDal.GetProductList(categoryId, subCategoryId, subSubCategoryId, pageLength, shortBy, currentPage, take, skip, idList.Cast<int?>().ToArray(), minPrice, maxPrice);
                    var models = productList.ProductModels.ToList();
                    productList.TotalProduct = models.Count();
                    productList.ProductModels = models.GroupBy(x => x.ProductId).Select(y => y.FirstOrDefault());
                }
                else
                {
                    productList.ProductModels = new List<ProductModel>();
                }
                
            }
            else
            {
                productList.TotalProduct = await _objProductDal.GetTotalProductCount(categoryId, subCategoryId, subSubCategoryId);
                productList.ProductModels = await _objProductDal.GetProductList(categoryId, subCategoryId, subSubCategoryId, pageLength, shortBy, currentPage, take, skip, null, minPrice, maxPrice);
                productList.ProductModels = productList.ProductModels.GroupBy(x => x.ProductId).Select(y => y.FirstOrDefault());
                //productList.TotalProduct = productList.ProductModels.Count();
            }

            if (categoryId != null && categoryId != 0)
            {
                productList.CategoryId = categoryId;
                productList.CategoryName = await _objProductDal.GetCategoryName((int)categoryId);
            }
            if (categoryId != null && categoryId != 0 && subCategoryId != null && subCategoryId != 0)
            {
                productList.SubCategoryId = subCategoryId;
                productList.SubCategoryName = await _objProductDal.GetSubCategoryName((int)categoryId, (int)subCategoryId);
            }
            if (categoryId != null && categoryId != 0 && subCategoryId != null && subCategoryId != 0 && subSubCategoryId != null && subSubCategoryId != 0)
            {
                productList.SubSubCategoryId = subSubCategoryId;
                productList.SubSubCategoryName = await _objProductDal.GetSubSubCategoryName((int)categoryId, (int)subCategoryId, (int)subSubCategoryId);
            }

            return View(productList);
        }

        public async Task<ActionResult> ProductListSidebar(int? categoryId, int? subCategoryId, int? subSubCategoryId, string[] colorId, string[] sizeId, double? minPrice, double? maxPrice)
        {
            ProductListSideBar productListSideBar = new ProductListSideBar();
            productListSideBar.ProductModels = await _objProductDal.BestSellingProduct();
            productListSideBar.Colors = await _objProductDal.ColorFilterList(categoryId, subCategoryId, subSubCategoryId, colorId);
            productListSideBar.Sizes = await _objProductDal.SizeFilterList(categoryId, subCategoryId, subSubCategoryId, sizeId);

            if (minPrice != null && maxPrice != null && maxPrice > minPrice)
            {
                productListSideBar.MinPrice = minPrice;
                productListSideBar.MaxPrice = maxPrice;
            }

            productListSideBar.CategoryId = categoryId;
            productListSideBar.SubCategoryId = subCategoryId;
            productListSideBar.SubSubCategoryId = subSubCategoryId;

            productListSideBar.MenuMains = await _objHomeDal.GetMenuMain();

            foreach (var menuMain in productListSideBar.MenuMains)
            {
                menuMain.MenuSubs = await _objHomeDal.GetMenuSub(menuMain.MenuMainId);

                foreach (var menuSub in menuMain.MenuSubs)
                {
                    menuSub.MenuSubSubs = await _objHomeDal.GetMenuSubSub(menuMain.MenuMainId, menuSub.MenuSubId);
                }
            }

            return PartialView("ProductPagePartial/_SidebarPartial", productListSideBar);
        }

        public async Task<ActionResult> SingleProduct(int? productId)
        {
            if (productId != null && productId != 0)
            {
                var product = await _objProductDal.GetSingleProduct((int)productId);
                if (product != null && product.ProductId != 0)
                {
                    product.ProductModels = await _objProductDal.GetRelatedProduct(product.CategoryId, product.SubCategoryId, product.SubSubCategoryId);

                    product.RatioLists = await _objProductDal.GetRatioLists((int)productId);
                    var productRatioLists = product.RatioLists.ToList();
                    if (productRatioLists.Any())
                    {
                        foreach (var sizeList in productRatioLists)
                        {
                            sizeList.RatioSizeLists = await _objProductDal.GetRatioSizeLists((int)productId, sizeList.RatioId);
                        }
                    }

                    return View(product);
                }             
            }

            return RedirectToAction("ProductList", "Product");
        }

        public async Task<ActionResult> FastViewProduct(int productId)
        {
            var quickView = await _objProductDal.GetProductForQuickView(productId);

            if (quickView.ProductName == null)
            {
                ViewBag.ErrorMessage = "Something wrong, No product Found";
            }

            return PartialView("_FastViewProductPartial", quickView);
        }

        public async Task<ActionResult> GetInventoryValue(int productId, int colorId, int sizeId)
        {
            var quantity = await _objProductDal.GetInventoryValue(productId, colorId, sizeId);

            Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
            Response.AddHeader("Expires", "Fri, 01 Jan 1990 00:00:00 GMT");
            Response.AddHeader("Pragma", "no-cache");

            return Json(quantity, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetProductPrice(int productId, int colorId, int sizeId)
        {
            var price = await _objProductDal.GetProductPrice(productId, colorId, sizeId);

            Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
            Response.AddHeader("Expires", "Fri, 01 Jan 1990 00:00:00 GMT");
            Response.AddHeader("Pragma", "no-cache");

            return Json(price, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetSizeList(int productId)
        {
            var productSize = await _objProductDal.GetProductSizeList(productId);

            Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
            Response.AddHeader("Expires", "Fri, 01 Jan 1990 00:00:00 GMT");
            Response.AddHeader("Pragma", "no-cache");

            return Json(productSize, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetColorList(int productId)
        {
            var productColor = await _objProductDal.GetProductColorList(productId);

            Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
            Response.AddHeader("Expires", "Fri, 01 Jan 1990 00:00:00 GMT");
            Response.AddHeader("Pragma", "no-cache");

            return Json(productColor, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetImageList(int productId, int colorId)
        {
            var productColor = await _objProductDal.GetProductImageList(productId, colorId);

            Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
            Response.AddHeader("Expires", "Fri, 01 Jan 1990 00:00:00 GMT");
            Response.AddHeader("Pragma", "no-cache");

            return Json(productColor, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Cart()
        {
            CheckoutModel checkout = new CheckoutModel();
            return View(checkout);
        }

        public async Task<ActionResult> CustomerReview(int productId)
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.Message = TempData["message"] as string;
            }

            CustomerReview model = new CustomerReview
            {
                ProductId = productId
            };

            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

            if (user != null)
            {
                model.CustomerName = user.FirstName +" "+ user.LastName;
                model.CustomerEmail = user.Email;
                model.CustomerPhone = user.PhoneNo;
                model.CustomerId = user.Id;
            }

            ViewBag.ReviewList = await _objProductDal.GetProductReview(productId);

            return PartialView("ProductPagePartial/_ProductReviewPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveCustomerReview(CustomerReview objCustomerReview)
        {
            if (ModelState.IsValid)
            {
                string strMessage = await _objProductDal.SaveCustomerReview(objCustomerReview);
                TempData["message"] = strMessage;
            }

            return RedirectToAction("SingleProduct", "Product", new { productId = objCustomerReview.ProductId });
        }

        public async Task<ActionResult> SearchResult(string search)
        {
            if (string.IsNullOrWhiteSpace(search))
            {
                return RedirectToAction("ProductList", "Product");
            }

            var getProductIdList = await _objProductDal.GetSearchedProductId(search);
            IEnumerable<ProductModel> model = new List<ProductModel>();

            if (getProductIdList.Any())
            {
                var productIdList = string.Join(",", getProductIdList);

                model = await _objProductDal.GetSearchedProduct(productIdList);

                model = model.DistinctBy(c => c.ProductId).ToList();
            }

            ViewBag.RearchResult = search;
            return View(model);
        }

        public async Task<JsonResult> GetSearchHintsList(string query)
        {
            List<string> list = await _objProductDal.GetSearchHints(query);
            list = list.ConvertAll(d => d.ToUpper());
            list = list.Distinct().ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

    }
}