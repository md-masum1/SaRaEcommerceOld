﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NLog;
using SaRaEcommerce.Dal;
using SaRaEcommerce.Models;
using SaRaEcommerce.Utility;
using SaRaEcommerce.ViewModel;

namespace SaRaEcommerce.Controllers
{
    public class HomeController : Controller
    {
        private readonly HomeModel _objHomeModel = new HomeModel();
        private readonly HomeDal _objHomeDal = new HomeDal();

        //[OutputCache(Duration = 60, VaryByParam = "none", Location = System.Web.UI.OutputCacheLocation.Server)]
        public async Task<ActionResult> Index()
        {
            _objHomeModel.HomepageSliders = await _objHomeDal.GetHomepageSlider();
            _objHomeModel.HomepageBanners = await _objHomeDal.GetHomepageBanner();
            _objHomeModel.HomepageOffers = await _objHomeDal.GetHomepageOffers();

            var popupImage = await _objHomeDal.GetHomePagePopup();
            ViewBag.Popup = popupImage;

            return View(_objHomeModel);
        }

        [HttpGet]
        public async Task<ActionResult> UpcomingProduct()
        {
            var objUpcomingProduct = await _objHomeDal.GetUpcomingProduct();

            return PartialView("HomePagePartial/_UpcomingProductPartial", objUpcomingProduct);
        }

        public async Task<ActionResult> PlacePreOrder(int productId, string customerId)
        {
            string message = "";
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

            if (user != null)
            {
                if (customerId == user.Id && productId != 0)
                 message = await _objHomeDal.PlacePreOrder(productId, customerId);

                new Thread(() =>
                {
                    string body = "Your Pre-Order Successfully Placed, you will get another mail for confirmation and delivery date, for more information please contact with us ( +88 01885 998899).";

                    UtilityClass.SendEmail(user.Email, "SaRa Lifestyle - Pre-Order Status", body, null);
                }).Start();
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OutputCache(Duration = 60)]
        public async Task<ActionResult> OccasionalProduct()
        {
            var objOccasionalProduct = await _objHomeDal.GetOccasionalProducts();

            var occasionalSlider = await _objHomeDal.GetOccasionalSlider();
            ViewBag.OccasionalSlider = occasionalSlider;

            return PartialView("HomePagePartial/_OccasionalProductPartial", objOccasionalProduct);
        }

        [HttpGet]
        [OutputCache(Duration = 60)]
        public async Task<ActionResult> NewArrivals()
        {
            var objNewArrivalsProduct = await _objHomeDal.GetNewArrivalsProduct();

            return PartialView("HomePagePartial/_NewArrivalsPartial", objNewArrivalsProduct);
        }

        [HttpGet]
        [OutputCache(Duration = 60)]
        public async Task<ActionResult> TrendingProducts()
        {
            var objTrendingProduct = await _objHomeDal.GetTrendingProducts();

            return PartialView("HomePagePartial/_TrendingProductsPartial", objTrendingProduct);
        }

        [HttpGet]
        [OutputCache(Duration = 60)]
        public async Task<ActionResult> PromotionalProducts()
        {
            var objPromotionalProducts = await _objHomeDal.GetPromotionalProducts();

            return PartialView("HomePagePartial/_PromotionalProductPartial", objPromotionalProducts);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.Message = TempData["message"] as string;
            }
            ModelState.Clear();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(CustomerContact objContact)
        {
            string message;

            if (!ModelState.IsValid)
            {
                message = "Invalid";
                return RedirectToAction("Contact", "Home", new {message });
            }
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

            if (user != null)
            {
                objContact.CustomerId = user.Id;
            }

            message = await _objHomeDal.SaveCustomerMessage(objContact);

            new Thread(() =>
            {
                string body = "Thank you for your response, We will contact with you soon.";

                UtilityClass.SendEmail(objContact.CustomerEmail, "SaRa Life Style", body, null);
            }).Start();

            var emailList = new List<string> { "ecom@saralifestyle.com.bd" };

            foreach (var email in emailList)
            {
                new Thread(() =>
                {
                    string body = objContact.Message;

                    UtilityClass.SendEmail(email, objContact.CustomerName + " (" + objContact.CustomerEmail + ")", body, null);
                }).Start();
            }

            TempData["message"] = message;

            return RedirectToAction("Contact", "Home");
        }


        [ChildActionOnly]
        [OutputCache(Duration = 60)]
        public async Task<ActionResult> Menu()
        {
            var menuMains = await _objHomeDal.GetMenuMain();

            foreach (var menuMain in menuMains)
            {
                menuMain.MenuSubs = await _objHomeDal.GetMenuSub(menuMain.MenuMainId);

                foreach (var menuSub in menuMain.MenuSubs)
                {
                    menuSub.MenuSubSubs = await _objHomeDal.GetMenuSubSub(menuMain.MenuMainId, menuSub.MenuSubId);
                }
            }

            return PartialView("HomePagePartial/_MenuePartial", menuMains);
        }

        public ActionResult News()
        {
            return View();
        }

        public ActionResult Blog()
        {
            return View();
        }

        public ActionResult BlogItem()
        {
            return View();
        }
    }
}