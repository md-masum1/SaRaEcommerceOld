﻿using SaRaEcommerce.Models;
using SaRaEcommerce.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SaRaEcommerce.Dal;
using SaRaEcommerce.Utility;
using System.IO;
using System.Net.Http;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Rotativa;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using NLog;
using System.Web.Script.Serialization;

namespace SaRaEcommerce.Controllers
{
    public class CheckOutController : Controller
    {
        private readonly CheckoutDal _objCheckoutDal = new CheckoutDal();
        public readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly string _appKey = "shm6stbtthcoifho6s600699r";
        private readonly string _appSecret = "62q8nnurmeug9gd3us42nb3quktmn8rgtc2qjatlbo5i3th60kg";
        private readonly string _userName = "SARA";
        private readonly string _password = "S@aiR9a3L1f28M";


        private readonly ReportDocument _objReportDocument = new ReportDocument();
        private ExportFormatType _objExportFormatType = ExportFormatType.NoFormat;

        public byte[] ShowReport(string pFileDownloadName)
        {

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Clear();
            Response.Buffer = true;

            _objExportFormatType = ExportFormatType.PortableDocFormat;

            Stream oStream = _objReportDocument.ExportToStream(_objExportFormatType);
            byte[] byteArray = new byte[oStream.Length];
            oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));

            Response.ContentType = "application/pdf";

            pFileDownloadName += ".pdf";

            Response.BinaryWrite(byteArray);
            Response.Flush();
            Response.Close();
            _objReportDocument.Close();
            _objReportDocument.Dispose();

            //return File(oStream, Response.ContentType, pFileDownloadName);
            return byteArray;
        }

        private async Task<byte[]> GenerateSaleReport(string orderNumber)
        {
            string strPath = Path.Combine(Server.MapPath("~/Reports/Invoice.rpt"));
            _objReportDocument.Load(strPath);

            DataSet objDataSet = (await _objCheckoutDal.GetOrdererReport(orderNumber));


            _objReportDocument.Load(strPath);
            _objReportDocument.SetDataSource(objDataSet);
            _objReportDocument.SetDatabaseLogon("saraecom", "saraecom");

            

            byte[] pdfData = ShowReport("Invoice");

            return pdfData;
            
        }

        public JsonResult SaveCartInformation()
        {
            var data = new
            {
                isRedirect = true,
                redirectUrl = Url.Action("Index", "CheckOut")
            };

            return Json(data);
        }

        public ActionResult Index()
        {
            CheckoutModel model = new CheckoutModel();
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

            if (user != null)
            {
                model.Email = user.Email;
                model.FirstName = user.FirstName;
                model.LastName = user.LastName;
                model.Phone = user.PhoneNo;
                model.Address1 = user.Address1;
                model.Address2 = user.Address2;
                model.City = user.City.Trim();
                model.Country = user.Country;
                model.CustomerId = user.Id;
            }

            model.Country = "Bangladesh";
            return View(model);
        }

        public async Task<ActionResult> CheckOutConfirm(string orderNumber)
        {
            if (!string.IsNullOrWhiteSpace(orderNumber))
            {

                var orderInformation = await _objCheckoutDal.GetOrderInformation(orderNumber);

                if (orderInformation != null)
                {
                    orderInformation.CartProducts = await _objCheckoutDal.GetOrderedProduct(orderInformation.OrderNumber);
                }

                return View(orderInformation);
            }
            
            return null;
        }

        private bool ProductValidate(CheckoutModel checkout)
        {
            foreach (var orderItem in checkout.CartProducts)
            {
                var quantity = _objCheckoutDal.GetInventoryValue(orderItem.ProductId, orderItem.ColorId, orderItem.SizeId);
                var price = _objCheckoutDal.GetProductPrice(orderItem.ProductId, orderItem.ColorId, orderItem.SizeId);

                if (quantity > 0 && quantity >= orderItem.Quantity)
                {
                    if (price > 0 && price.Equals(orderItem.Price))
                    {
                        // success
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            
            return true;
        }

        private bool ValidateIP(string ipString)
        {
            if (String.IsNullOrWhiteSpace(ipString))
            {
                return false;
            }

            string[] splitValues = ipString.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            byte tempForParsing;

            return splitValues.All(r => byte.TryParse(r, out tempForParsing));
        }

        [HttpPost]
        public async Task<ActionResult> SaveCheckout(CheckoutModel checkout)
        {
            string message = "";

            if (!ModelState.IsValid)
            {
                message = "something went wrong";
                return Json(message);
            }

            if (!checkout.CartProducts.Any())
            {
                message = "Cart can not be empty";
                return Json(message);
            }

            foreach (var cartProduct in checkout.CartProducts)
            {
                if (cartProduct.ProductId == 0 && cartProduct.ColorId == 0 && cartProduct.SizeId == 0 &&
                    cartProduct.Quantity == 0)
                {
                    return RedirectToAction("Index", "CheckOut");
                }
            }

            if (!ProductValidate(checkout))
            {
                message = "invalid information, please remove your cart item and try again...";
                return Json(message);
            }

            dutchbanglabank.dbblecomtxn client = new dutchbanglabank.dbblecomtxn();
           

            string amount = (Convert.ToInt32(checkout.TotalAmount) * 100).ToString();

            checkout.CustomerIp = HttpContext.Request.UserHostAddress;

            if (!ValidateIP(checkout.CustomerIp))
                checkout.CustomerIp = "192.168.2.10";

            string pwd = UtilityClass.Pwd;
            string txnrefnum = "123456";
            string userid = UtilityClass.UserId;

            string substring = null;

            var orderNumber = await _objCheckoutDal.CheckoutCustomerSave(checkout);
            checkout.OrderNumber = orderNumber;

            if (!string.IsNullOrWhiteSpace(orderNumber))
            {
                foreach (var cartProduct in checkout.CartProducts)
                {
                    cartProduct.OrderNumber = orderNumber;
                   message = await _objCheckoutDal.CheckoutProductSave(cartProduct);
                }

                using (client)
                {
                    //System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                    if(checkout.CardType != "0" && Convert.ToInt32(checkout.CardType) < 7)
                    {
                        string trnId = client.getransid(userid, pwd, amount, checkout.CardType, txnrefnum, checkout.CustomerIp);

                        substring = trnId.Remove(0, 15);
                        checkout.TransId = substring;
                        await _objCheckoutDal.TransactionIdUpdate(checkout.OrderNumber, checkout.TransId);                       
                        Logger.Info("trn_id: " + trnId);
                    }
                    else
                    {
                        if (Convert.ToInt32(checkout.CardType) == 7)
                        {
                            checkout.TransSuccessStatus = "Y";
                            checkout.BkashPaymentResponse.OrderNumber = orderNumber;
                            await _objCheckoutDal.BkashPaymentSave(checkout.BkashPaymentResponse);
                        }
                            

                        await SaveCheckOutData(checkout);
                        var data = new
                        {
                            isRedirect = true,
                            redirectUrl = Url.Action("Index", "Home")
                        };

                        return Json(data);
                    }
                }

                
            }

            if (!string.IsNullOrWhiteSpace(substring))
            {
                var data = new
                {
                    isRedirect = true,
                    redirectUrl = "https://ecom.dutchbanglabank.com/ecomm2/ClientHandler?card_type=" + checkout.CardType + "&trans_id=" + HttpUtility.UrlEncode(substring)
                };

                return Json(data);
            }
            else
            {
                var data = new
                {
                    isRedirect = false,
                    redirectUrl = "https://ecom.dutchbanglabank.com/ecomm2/ClientHandler?card_type=" + checkout.CardType + "&trans_id=" + HttpUtility.UrlEncode(substring)
                };

                return Json(data);
            }
        }

        public async Task<ActionResult> CheckOutSuccess(string TRANS_ID)
        {
            Logger.Info("DBBL Return trn Id: " + new JavaScriptSerializer().Serialize(TRANS_ID));

            if (TRANS_ID != null)
            {
                var checkout = await _objCheckoutDal.GetOrderInformationByTrnId(TRANS_ID);

                Logger.Info("checkout info: " + new JavaScriptSerializer().Serialize(checkout));

                dutchbanglabank.dbblecomtxn client = new dutchbanglabank.dbblecomtxn();

                if (string.IsNullOrWhiteSpace(checkout.LastName))
                    checkout.LastName = checkout.FirstName;
                if (string.IsNullOrWhiteSpace(checkout.City))
                    checkout.City = "Dhaka";

                string billingInfo = "FIRSTNAME:" + checkout.FirstName + "^" + "LASTNAME:" + checkout.LastName + 
                    "^" + "STREET:BD^CITY:" + checkout.City + "^" + "STATE:BD^POSTALCODE:1216^COUNTRY:BD^EMAIL:" + 
                    checkout.Email + "^" + "PHONENUMBER:" + checkout.Phone + "^" + "VIPCUSTOMER:NO"; 

                var result = client.getresultfield(UtilityClass.UserId, UtilityClass.Pwd, checkout.TransId, checkout.CustomerIp, billingInfo);

                Logger.Info("responce: " + result);

                string resultStatus = (result.Split('>')[1]).Substring(0, 2);
                string resultCode = (result.Split('>')[2]).Substring(0, 3);

                if (resultStatus.ToUpper() == "OK")
                {
                    checkout.TransSuccessStatus = "Y";
                    await SaveCheckOutData(checkout);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    resultStatus = (result.Split('>')[1]).Substring(0, 6);
                    checkout.TransSuccessStatus = "N";
                    await SaveCheckOutData(checkout);
                    return RedirectToAction("CheckOutFailed", "CheckOut");
                }               
            }
            Logger.Info("DBBL return no trn id");
            return RedirectToAction("CheckOutFailed", "CheckOut");
        }

        private async Task<string> SaveCheckOutData(CheckoutModel checkout)
         {
            string message = "";
            var orderNumber = await _objCheckoutDal.CheckoutCustomerUpdate(checkout);

            if (!string.IsNullOrWhiteSpace(orderNumber) && orderNumber != "0")
            {
                var actionResult = new ActionAsPdf("CheckOutConfirm", new { orderNumber = orderNumber }) { FileName = "Invoice.pdf", PageMargins = { Left = 1, Right = 1 } };
                byte[] applicationPdfData = actionResult.BuildFile(ControllerContext);

                new Thread(() =>
                {
                    string body = "Order Place: Hello Mr " + checkout.FirstName + " " + checkout.LastName + ", " +
                                  "Your order (" + orderNumber + ") has been placed on "+ DateTime.Now.ToString("f") +". " +
                                  "You will be notified with another email after your order is confirmed. " +
                                  "Thanks for shopping with SaRa.Check your status here: (https://www.saralifestyle.com.bd/CustomerProfile/Dashboard)";
                    UtilityClass.SendSms(checkout.Phone, body);

                    UtilityClass.SendEmail(checkout.Email, "SaRa Life Style (" + orderNumber + ")", body, applicationPdfData);

                }).Start();

                Session["CheckOutInfo"] = null;
                return message;
            }
            return message;
        }

        public ActionResult CheckOutFailed()
        {
            if (Session["CheckOutInfo"] is CheckoutModel checkout)
            {
                Session["CheckOutInfo"] = null;
            }

            var responce = Session["responce"];

            ViewBag.responce = responce;

            return View();
        }

        public ActionResult GetEmployeeInfo(string employeeId)
        {
            var employee = new EmployeeModel();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(UtilityClass.ErpHostAddress);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("EmpForECom?vEmployeeId="+ employeeId)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();
                    var result = JsonConvert.DeserializeObject<IList<EmployeeModel>>(jsonstring.Result);

                    if(result.Count > 0 && result[0].Id == employeeId)
                        employee = result[0];
                }
            }
            return Json(employee, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VerifyEmployee(string userId, string userPhone)
        {
            if (!string.IsNullOrEmpty(userPhone) && !string.IsNullOrWhiteSpace(userId))
            {
                string smsBody = "Your verification code is " + userId;
                UtilityClass.SendSms(userPhone, smsBody);
                return Json("0", JsonRequestBehavior.AllowGet);
            }
            return Json("1", JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> VerifyPrivilegeCard(string cardNumber)
        {
            if (!string.IsNullOrEmpty(cardNumber))
            {
                var result = await _objCheckoutDal.VerifyPrivilegeCard(cardNumber);
                if (result > 0)
                {
                    var data = new
                    {
                        Discount = result,
                        CardNumber = cardNumber
                    };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        #region bKash Checkout Section

        public JsonResult GetIdTokenAndAppKey()
        {
            var token = GrantToken();
            if (!string.IsNullOrWhiteSpace(token.appKey) && !string.IsNullOrWhiteSpace(token.id_token))
            {
                return Json(token, JsonRequestBehavior.AllowGet);
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        private TokenModel GrantToken()
        {
            TokenModel token = new TokenModel();
            var body = new
            {
                app_key = _appKey,
                app_secret = _appSecret
            };

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            string jsonString = javaScriptSerializer.Serialize(body);

            HttpContent content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            content.Headers.Add("username", _userName);
            content.Headers.Add("password", _password);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://checkout.pay.bka.sh/v1.2.0-beta/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = Task.Run(async () => await client.PostAsync("checkout/token/grant", content)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();

                    token = JsonConvert.DeserializeObject<TokenModel>(jsonstring.Result);
                }
            }
            token.appKey = _appKey;
            return token;
        }

        private TokenModel RefreshToken(TokenModel model)
        {
            TokenModel token = new TokenModel();

            var body = new
            {
                app_key = _appKey,
                app_secret = _appSecret,
                refresh_token = model.refresh_token
            };

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            string jsonString = javaScriptSerializer.Serialize(body);

            HttpContent content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            content.Headers.Add("username", _userName);
            content.Headers.Add("password", _password);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://checkout.pay.bka.sh/v1.2.0-beta/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = Task.Run(async () => await client.PostAsync("checkout/token/refresh", content)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();

                    token = JsonConvert.DeserializeObject<TokenModel>(jsonstring.Result);
                }
            }
            return token;
        }

        public ActionResult CreatePayment(RequestPayment request)
        {
            CreatePayment payment = new CreatePayment();

            var body = new
            {
                amount = request.amount,
                currency = request.currency,
                intent = request.intent,
                merchantInvoiceNumber = request.merchantInvoiceNumber
            };

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            string jsonString = javaScriptSerializer.Serialize(body);

            HttpContent content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            //content.Headers.Add("Authorization", request.Authorization);
            content.Headers.Add("X-APP-Key", request.appKey);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://checkout.pay.bka.sh/v1.2.0-beta/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(request.Authorization);
                HttpResponseMessage response = Task.Run(async () => await client.PostAsync("checkout/payment/create", content)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();

                    payment = JsonConvert.DeserializeObject<CreatePayment>(jsonstring.Result);
                }
            }
            return Json(payment, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExecutePayment(ExecutePayment request)
        {
            ExecutePaymentResponse payment = new ExecutePaymentResponse();

            var body = new { };

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            string jsonString = javaScriptSerializer.Serialize(body);

            HttpContent content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            //content.Headers.Add("Authorization", request.Authorization);
            content.Headers.Add("X-APP-Key", request.AppKey);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://checkout.pay.bka.sh/v1.2.0-beta/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(request.Authorization);
                HttpResponseMessage response = Task.Run(async () => await client.PostAsync("checkout/payment/execute/" + request.paymentID, content)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();

                    payment = JsonConvert.DeserializeObject<ExecutePaymentResponse>(jsonstring.Result);

                    //QueryPayment qPament = new QueryPayment();
                    //qPament.id_token = request.Authorization;
                    //qPament.paymentID = payment.paymentID;
                    //QueryPayment(qPament);
                }
            }
            return Json(payment, JsonRequestBehavior.AllowGet);
        }

        private QueryPayment QueryPayment(QueryPayment queryPayment)
        {
            QueryPayment token = new QueryPayment();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://checkout.pay.bka.sh/v1.2.0-beta/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(queryPayment.id_token);
                client.DefaultRequestHeaders.Add("X-APP-Key", _appKey);

                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("checkout/payment/query/" + queryPayment.paymentID)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();

                    token = JsonConvert.DeserializeObject<QueryPayment>(jsonstring.Result);

                    TransactionDetails td = new TransactionDetails();
                    td.id_token = queryPayment.id_token;
                    td.trxID = token.trxID;
                    TransactionDetails(td);
                }
            }
            return token;
        }

        private TransactionDetails TransactionDetails(TransactionDetails transactionDetails)
        {
            TransactionDetails token = new TransactionDetails();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://checkout.pay.bka.sh/v1.2.0-beta/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(transactionDetails.id_token);
                client.DefaultRequestHeaders.Add("X-APP-Key", _appKey);

                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("checkout/payment/search/" + transactionDetails.trxID)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonstring = response.Content.ReadAsStringAsync();
                    jsonstring.Wait();
                    var token2 = GrantToken();
                    token = JsonConvert.DeserializeObject<TransactionDetails>(jsonstring.Result);
                }
            }
            return token;
        }

        #endregion
    }
}