namespace SaRaEcommerce.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "SARAECOM.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "SARAECOM.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("SARAECOM.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("SARAECOM.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "SARAECOM.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(maxLength: 256),
                        LastName = c.String(maxLength: 256),
                        PhoneNo = c.String(maxLength: 20),
                        Gender = c.String(maxLength: 6),
                        Address1 = c.String(maxLength: 500),
                        Address2 = c.String(maxLength: 500),
                        City = c.String(maxLength: 50),
                        Country = c.String(maxLength: 50),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Decimal(nullable: false, precision: 1, scale: 0),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Decimal(nullable: false, precision: 1, scale: 0),
                        TwoFactorEnabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Decimal(nullable: false, precision: 1, scale: 0),
                        AccessFailedCount = c.Decimal(nullable: false, precision: 10, scale: 0),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "SARAECOM.AspNetUserClaims",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("SARAECOM.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "SARAECOM.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("SARAECOM.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("SARAECOM.AspNetUserRoles", "UserId", "SARAECOM.AspNetUsers");
            DropForeignKey("SARAECOM.AspNetUserLogins", "UserId", "SARAECOM.AspNetUsers");
            DropForeignKey("SARAECOM.AspNetUserClaims", "UserId", "SARAECOM.AspNetUsers");
            DropForeignKey("SARAECOM.AspNetUserRoles", "RoleId", "SARAECOM.AspNetRoles");
            DropIndex("SARAECOM.AspNetUserLogins", new[] { "UserId" });
            DropIndex("SARAECOM.AspNetUserClaims", new[] { "UserId" });
            DropIndex("SARAECOM.AspNetUsers", "UserNameIndex");
            DropIndex("SARAECOM.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("SARAECOM.AspNetUserRoles", new[] { "UserId" });
            DropIndex("SARAECOM.AspNetRoles", "RoleNameIndex");
            DropTable("SARAECOM.AspNetUserLogins");
            DropTable("SARAECOM.AspNetUserClaims");
            DropTable("SARAECOM.AspNetUsers");
            DropTable("SARAECOM.AspNetUserRoles");
            DropTable("SARAECOM.AspNetRoles");
        }
    }
}
