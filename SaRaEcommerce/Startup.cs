﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SaRaEcommerce.Startup))]
namespace SaRaEcommerce
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
