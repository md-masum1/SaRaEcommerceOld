﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SaRaEcommerce.ViewModel;

namespace SaRaEcommerce.Utility
{
    public class UtilityClass
    {
        public static string ErpHostAddress = "http://182.160.113.109/Api/";
        public static string Pwd = "4bd9837ddfe99a4e17a608685acdec3f7411274d";
        public static string UserId = "000599991330000";
        //For dropdown list
        public static SelectList GetSelectListByDataTable(DataTable objDataTable, string pValueField, string pTextField)
        {
            List<SelectListItem> objSelectListItems = new List<SelectListItem>
            {
                new SelectListItem() {Value = "", Text = @"Please select one"}
            };


            objSelectListItems.AddRange(from DataRow dataRow in objDataTable.Rows
                                        select new SelectListItem()
                                        {
                                            Value = dataRow[pValueField].ToString(),
                                            Text = dataRow[pTextField].ToString()
                                        });

            return new SelectList(objSelectListItems, "Value", "Text");
        }

        public static SelectList ProductPageLengthItem()
        {
            
            var list = new List<SelectListItem>
            {
                new SelectListItem{ Text=@"48", Value = "48" },
                new SelectListItem{ Text=@"64", Value = "64" },
                new SelectListItem{ Text=@"100", Value = "100" }
            };

            return new SelectList(list , "Value", "Text");
        }

        public static SelectList ReturnReasonItem()
        {

            var list = new List<SelectListItem>
            {
                new SelectListItem{ Text=@"---Select Reason ---", Value = "" },
                new SelectListItem{ Text=@"Wrong Size", Value = "Wrong Size" },
                new SelectListItem{ Text=@"Not As Expected", Value = "Not As Expected" },
                new SelectListItem{ Text=@"Defect", Value = "Defect" },
                new SelectListItem{ Text=@"Other", Value = "Other" }
            };

            return new SelectList(list, "Value", "Text");
        }

        public static SelectList ExchangeTypeItem()
        {

            var list = new List<SelectListItem>
            {
                new SelectListItem{ Text=@"--- Select Exchange ---", Value = "" },
                new SelectListItem{ Text=@"Refund", Value = "Refund" },
                new SelectListItem{ Text=@"Other Sizes", Value = "Other Sizes" },
                new SelectListItem{ Text=@"Other Product", Value = "Other Product" }
            };

            return new SelectList(list, "Value", "Text");
        }

        public static SelectList ProductPageShortItem()
        {
            var list = new List<SelectListItem>
            {
                new SelectListItem{ Text=@"Default", Value = "0" },
                new SelectListItem{ Text=@"Name (A - Z)", Value = "1" },
                new SelectListItem{ Text=@"Name (Z - A)", Value = "2" },
                new SelectListItem{ Text=@"Price (Low to High)", Value = "3" },
                new SelectListItem{ Text=@"Price (High to Low)", Value = "4" },
                new SelectListItem{ Text=@"Product (Old to New)", Value = "5" },
                new SelectListItem{ Text=@"Product (New to Old)", Value = "6" }
            };

            return new SelectList(list, "Value", "Text");
        }

        //For save an image
        public static string SaveBase64Image(HttpPostedFileBase image)
        {
            var imageExtension = Path.GetExtension(image.FileName);
            string base64Image = null;

            if (imageExtension != null)
            {
                imageExtension = imageExtension.ToUpper();

                if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                {
                    Stream str = image.InputStream;
                    BinaryReader br = new BinaryReader(str);
                    Byte[] fileDet = br.ReadBytes((Int32)str.Length);
                    base64Image = Convert.ToBase64String(fileDet);
                }
            }
            return base64Image;
        }

        //For File to byteArray
        public static Byte[] FileToByteArray(HttpPostedFileBase file)
        {
            var fileExtension = Path.GetExtension(file.FileName);
            Byte[] byteArray = null;

            if (fileExtension != null)
            {
                fileExtension = fileExtension.ToUpper();

                if (fileExtension == ".PDF")
                {
                    Stream str = file.InputStream;
                    BinaryReader br = new BinaryReader(str);
                    byteArray = br.ReadBytes((Int32)str.Length);
                }
            }
            return byteArray;
        }

        //For display an image Compressed
        public static string GetBase64ImageCompressed(byte[] image)
        {
            string base64Image = null;

            if (image != null)
            {
                MemoryStream myMemStream = new MemoryStream(image);
                Image fullSizeImage = Image.FromStream(myMemStream);
                Image newImage = fullSizeImage.GetThumbnailImage(50, 50, null, IntPtr.Zero);
                MemoryStream myResult = new MemoryStream();
                newImage.Save(myResult, ImageFormat.Png);
                image = myResult.ToArray();

                base64Image = "data:image/png;base64," + Convert.ToBase64String(image);
            }
            return base64Image;
        }

        //For display an image
        public static string GetBase64Image(byte[] image)
        {
            string base64Image = null;

            if (image != null)
            {
                MemoryStream myMemStream = new MemoryStream(image);
                Image fullSizeImage = Image.FromStream(myMemStream);
                MemoryStream myResult = new MemoryStream();
                fullSizeImage.Save(myResult, ImageFormat.Png);
                image = myResult.ToArray();

                base64Image = "data:image/png;base64," + Convert.ToBase64String(image);
            }
            return base64Image;
        }

        //For Encrypt an Text
        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        //For Decrypt an Text
        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static bool SendEmail(string toEmail, string subject, string body, byte[] invoiceData)
        {
            try
            {
                string senderEmail = "ecom@saralifestyle.com.bd";

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                var credential = new NetworkCredential()
                {
                    UserName = "ecom@saralifestyle.com.bd",
                    Password = "ScO16%}]yCn;,@!"
                };
                client.Credentials = credential;

                MailMessage mailMessage = new MailMessage(senderEmail, toEmail, subject, body);
                mailMessage.IsBodyHtml = true;

                if (invoiceData != null)
                {
                    Guid guid = new Guid();
                    byte[] applicationPdfData = invoiceData;
                    Attachment attPdf = new Attachment(new MemoryStream(applicationPdfData), guid + ".pdf");
                    mailMessage.Attachments.Add(attPdf);
                }
                mailMessage.BodyEncoding = Encoding.UTF8;

                client.Send(mailMessage);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool SendEmailCareer(string toEmail, string subject, string body, byte[] invoiceData)
        {
            try
            {
                string senderEmail = "recruitment@snowtex.org";

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                var credential = new NetworkCredential()
                {
                    UserName = "recruitment@snowtex.org",
                    Password = "ScO16%}]yCn;,@"
                };
                client.Credentials = credential;

                MailMessage mailMessage = new MailMessage(senderEmail, toEmail, subject, body);
                mailMessage.IsBodyHtml = true;

                if (invoiceData != null)
                {
                    Guid guid = new Guid();
                    byte[] applicationPdfData = invoiceData;
                    Attachment attPdf = new Attachment(new MemoryStream(applicationPdfData), guid + ".pdf");
                    mailMessage.Attachments.Add(attPdf);
                }
                mailMessage.BodyEncoding = Encoding.UTF8;

                client.Send(mailMessage);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool SendSms(string toNumber, string smsBody)
        {
            string baseAddress = "https://api.mobireach.com.bd/";
            string url = "SendTextMessage?Username=sara&Password=Lifestyle@123&From=SaRa&To="+ toNumber.Trim() +"&Message=" + smsBody.Trim();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);

                var responseTask = client.GetAsync(url);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
            }

            return false;
        }

        public static string GetIpAddress()
        {
            var IPAddress = "";
            IPHostEntry Host = default(IPHostEntry);
            string Hostname = null;
            Hostname = System.Environment.MachineName;
            Host = Dns.GetHostEntry(Hostname);
            foreach (IPAddress IP in Host.AddressList)
            {
                if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    IPAddress = Convert.ToString(IP);
                }
            }
            return IPAddress;
        }
    }
}