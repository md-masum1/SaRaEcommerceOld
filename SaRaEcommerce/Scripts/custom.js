﻿function loadTopCart() {
    var retrievedCartObject = JSON.parse(localStorage.getItem('cartObjectArray'));

    if (retrievedCartObject != null) {
        var cartQuantity = retrievedCartObject.length;
        var cartTotalPrice = 0;
        $("#cartSummeryBody").html("");
        for (var i = 0; i < retrievedCartObject.length; i++) {
            cartTotalPrice = cartTotalPrice + retrievedCartObject[i].totalPrice;

            $("#cartSummeryBody").append("<li>");
            $("#cartSummeryBody").append("<a href='#'><img src='" + retrievedCartObject[i].productImage + "' alt='" + retrievedCartObject[i].productName + "' width='37' height='34'></a>");
            $("#cartSummeryBody").append("<span class='cart-content-count'>x " + retrievedCartObject[i].quantity + "</span>");
            $("#cartSummeryBody").append("<strong><a href='/Product/SingleProduct?productId=" + retrievedCartObject[i].productId +"'>" + retrievedCartObject[i].productName + " (" + retrievedCartObject[i].colorName + " - " + retrievedCartObject[i].sizeName +")</a></strong>");
            $("#cartSummeryBody").append("<em>&#2547 " + retrievedCartObject[i].totalPrice + "</em>");
            //$("#cartSummeryBody").append("<a href='#' class='del-goods'>&nbsp;</a>");
            $("#cartSummeryBody").append("<a class='del-goods' href='javascript:;' data-color='" +
                retrievedCartObject[i].colorId +
                "' data-size='" +
                retrievedCartObject[i].sizeId +
                "' data-id='" +
                retrievedCartObject[i].productId +
                "'>&nbsp;</a>");

            $("#cartSummeryBody").append("</li>");
        }

        $("#cartSummeryTop").html("");
        $("#cartSummeryTop").append("<a href='javascript:void(0);'class='top-cart-info-count'>" + cartQuantity + " items</a>");
        $("#cartSummeryTop").append("<a href='javascript:void(0);'class='top-cart-info-value'>&#2547 " + cartTotalPrice + "</a>");

    } else {
        $("#cartSummeryTop").html("");
        $("#cartSummeryTop").append("<a href='javascript:void(0);'class='top-cart-info-count'>0 items</a>");
        $("#cartSummeryTop").append("<a href='javascript:void(0);'class='top-cart-info-value'>&#2547 0</a>");

    }
}

function loadCartMainBody() {
    var retrievedCartObject = JSON.parse(localStorage.getItem('cartObjectArray'));

    if (retrievedCartObject != null) {
        var cartQuantity = retrievedCartObject.length;
        var shippingCost = 0;
        var cartTotalPrice = 0;
        $("#cartTable").html("");
        $("#cartSummery").html("");
        $("#cartTable").append("<tr>" +
            "<th class='goods-page-image'>Image</th>" +
            "<th class='goods-page-description'>Description</th>" +
            "<th class='goods-page-ref-no'>SKU</th>" +
            "<th class='goods-page-quantity'> Quantity</th >" +
            "<th class='goods-page-price'> Unit price</th>" +
            "<th class='goods-page-total' colspan='2'> Total</th > " +
            "</tr>");
        for (var i = 0; i < cartQuantity; i++) {
            cartTotalPrice = cartTotalPrice + retrievedCartObject[i].totalPrice;

            $("#cartTable").append("<tr>" +
                "<td class='goods-page-image'><a href='javascript:;'><img src='" + retrievedCartObject[i].productImage + "' alt='" + retrievedCartObject[i].productName + "'></a></td>" +
                "<td class='goods-page-description'>" +
                "<h3><a href='/Product/SingleProduct?productId=" + parseInt(retrievedCartObject[i].productId) + "'>" + retrievedCartObject[i].productName + "</a></h3>" +
                "<p>Color: " + retrievedCartObject[i].colorName + "; Size: " + retrievedCartObject[i].sizeName + "</p>" +
                "</td>" +
                "<td class='goods-page-ref-no'>" + retrievedCartObject[i].sku + "</td>" +
                "<td class='goods-page-quantity'>" +
                "<div class='product-quantity'>" +
                "<input id='product-quantity' type='text' value='" + retrievedCartObject[i].quantity + "' data-availableQuantity='" + retrievedCartObject[i].availableQuantity + "' data-price='" + retrievedCartObject[i].price + "' data-color='" + retrievedCartObject[i].colorId + "' data-size='" + retrievedCartObject[i].sizeId + "' data-id='" + retrievedCartObject[i].productId + "' readonly class='form-control input-sm'>" +
                "</div>" +
                "</td>" +
                "<td class='goods-page-price'><strong><span>&#2547</span>" + retrievedCartObject[i].price + "</strong></td>" +
                "<td class='goods-page-total'><strong><span>&#2547</span>" + retrievedCartObject[i].totalPrice + "</strong></td>" +
                "+<td class='del-goods-col'>" +
                "<a class='del-goods' href='javascript:;' data-color='" + retrievedCartObject[i].colorId + "' data-size='" + retrievedCartObject[i].sizeId + "' data-id='" + retrievedCartObject[i].productId + "'>&nbsp;</a>" +
                "<input type='hidden' class='productId' value='" + parseInt(retrievedCartObject[i].productId) + "'>" +
                "</td>" +
                "</tr>");
        }

        $("#cartSummery").append("<ul>" +
            "<li>" +
            "<em>Sub total</em>" +
            "<strong class='price'><span>&#2547</span>" + cartTotalPrice + "</strong>" +
            "</li>" +
            "<li>" +
            "<em> Shipping cost</em>" +
            "<strong class='price'><span>&#2547</span>" + shippingCost + "</strong>" +
            "</li>" +
            "<li class='shopping-total-price'>" +
            "<em>Total</em> " +
            "<strong class='price'><span>&#2547</span>" + parseFloat(cartTotalPrice + shippingCost) + "</strong>" +
            "</li>" +
            "</ul>");
        $('#btnCheckout').prop('disabled', false);

    } else {
        $("#cartTable").html("");
        $("#cartSummery").html("");
        $("#cartTable").append("<tr>" +
            "<th class='goods-page-image'>Image</th>" +
            "<th class='goods-page-description'>Description</th>" +
            "<th class='goods-page-ref-no'>SKU</th>" +
            "<th class='goods-page-quantity'> Quantity</th >" +
            "<th class='goods-page-price'> Unit price</th>" +
            "<th class='goods-page-total' colspan='2'> Total</th > " +
            "</tr>");
        $("#cartSummery").append("<ul>" +
            "<li>" +
            "<em>Sub total</em>" +
            "<strong class='price'><span>&#2547</span>0</strong>" +
            "</li>" +
            "<li>" +
            "<em> Shipping cost</em>" +
            "<strong class='price'><span>&#2547</span>0</strong>" +
            "</li>" +
            "<li class='shopping-total-price'>" +
            "<em>Total</em> " +
            "<strong class='price'><span>&#2547</span>0</strong>" +
            "</li>" +
            "</ul>");
        $('#btnCheckout').prop('disabled', true);
    }
}

function loadMainCart() {

    loadCartMainBody();

    //$('#cartTable').on('click touchstart', '.bootstrap-touchspin-up', function () {
    //    var quantity = $(this).parents(".input-group-btn").siblings(".input-sm").val();
    //    var availableQuantity = $(this).parents(".input-group-btn").siblings(".input-sm").data("availablequantity");
    //    $(this).parents(".input-group-btn").siblings(".input-group-btn").find('.bootstrap-touchspin-down').prop('disabled', false);

    //    if (quantity >= availableQuantity) {
    //        $(this).prop('disabled', true);
    //        $(this).parents(".input-group-btn").siblings(".input-sm").val(availableQuantity);
    //    }
    //    else {
    //        $(this).prop('disabled', false);
    //    }
    //});

    //$('#cartTable').on('click touchstart', '.bootstrap-touchspin-down', function () {
    //    var quantity = $(this).parents(".input-group-btn").siblings(".input-sm").val();
    //    $(this).parents(".input-group-btn").siblings(".input-group-btn").find('.bootstrap-touchspin-up').prop('disabled', false);

    //    if (quantity <= 1) {
    //        $(this).prop('disabled', true);
    //        $(this).parents(".input-group-btn").siblings(".input-sm").val(1);
    //    } else {
    //        $(this).prop('disabled', false);
    //    }
    //});

    //$('.bootstrap-touchspin-up').on('click touchstart', function () {
    //    alert("hi");
    //        var quantity = $(this).parents(".input-group-btn").siblings(".input-sm").val();
    //        var availableQuantity = $(this).parents(".input-group-btn").siblings(".input-sm").data("availablequantity");
    //        $(this).parents(".input-group-btn").siblings(".input-group-btn").find('.bootstrap-touchspin-down')
    //            .prop('disabled', false);

    //        if (quantity >= availableQuantity) {
    //            $(this).prop('disabled', true);
    //            $(this).parents(".input-group-btn").siblings(".input-sm").val(availableQuantity);
    //        } else {
    //            $(this).prop('disabled', false);
    //        }
        
    //});

    //$('.bootstrap-touchspin-up').on('click touchstart', function () {
    //    alert("hi");
    //        var quantity = $(this).parents(".input-group-btn").siblings(".input-sm").val();
    //        $(this).parents(".input-group-btn").siblings(".input-group-btn").find('.bootstrap-touchspin-up')
    //            .prop('disabled', false);

    //        if (quantity <= 1) {
    //            $(this).prop('disabled', true);
    //            $(this).parents(".input-group-btn").siblings(".input-sm").val(1);
    //        } else {
    //            $(this).prop('disabled', false);
    //        }
    //});

    $('#cartTable').on('change', '.input-sm', function () {
        var quantity = parseInt($(this).val());
        var price = parseFloat($(this).data("price"));
        var availableQuantity = $(this).data("availablequantity");

        var productId = $(this).data("id");
        var colorId = $(this).data("color");
        var sizeId = $(this).data("size");

        

        if (quantity > 0 && quantity <= availableQuantity) {

            var cartObjectArray = JSON.parse(localStorage.getItem('cartObjectArray'));

            for (var j = 0; j < cartObjectArray.length; j++) {
                if (cartObjectArray[j].productId == productId &&
                    cartObjectArray[j].colorId == colorId &&
                    cartObjectArray[j].sizeId == sizeId) {

                    console.log(productId, colorId, sizeId);
                    localStorage.removeItem("cartObjectArray");

                    cartObjectArray[j].quantity = quantity;
                    cartObjectArray[j].totalPrice = parseFloat(price * quantity);

                    localStorage.setItem('cartObjectArray', JSON.stringify(cartObjectArray));
                    loadCartMainBody();
                    Layout.initTouchspin();
                    loadTopCart();
                }
            }
        } else {
            if (quantity < 1) {
                $(this).val(1);
            }
            if (quantity > availableQuantity) {
                $(this).val(availableQuantity);
            }
        }
    });

    $('#cartTable').on('click', '.del-goods', function () {
        var productId = $(this).data("id");
        var colorId = $(this).data("color");
        var sizeId = $(this).data("size");

        console.log(productId, colorId, sizeId);

        var cartObjectArray = JSON.parse(localStorage.getItem('cartObjectArray'));

        for (var j = 0; j < cartObjectArray.length; j++) {
            if (cartObjectArray[j].productId == productId &&
                cartObjectArray[j].colorId == colorId &&
                cartObjectArray[j].sizeId == sizeId) {
                console.log(productId, colorId, sizeId);
                localStorage.removeItem("cartObjectArray");
                cartObjectArray.splice(j, 1);
                localStorage.setItem('cartObjectArray', JSON.stringify(cartObjectArray));
                loadCartMainBody();
                Layout.initTouchspin();
                loadTopCart();
            }
        }
        if (cartObjectArray.length <= 0) {
            localStorage.removeItem("cartObjectArray");
        }
    });
}

function checkoutSummery(discount, pShippingCost) {
    var retrievedCartObject = JSON.parse(localStorage.getItem('cartObjectArray'));

    var parsedDataCart = JSON.parse(localStorage.getItem('singleProduct'));

    if (parsedDataCart != null) {
        retrievedCartObject = [];
        retrievedCartObject.push(parsedDataCart);
    }

    if (retrievedCartObject != null) {
        var cartQuantity = retrievedCartObject.length;
        var shippingCost = 0;
        if (pShippingCost != null) {
            shippingCost = pShippingCost;
        }
        var cartTotalPrice = 0;
        var discountPercent = discount;
        $("#cartTable").html("");
        $("#cartSummery").html("");
        $("#cartTable").append("<tr>" +
            "<th class='checkout-image'>Image</th>" +
            "<th class='checkout-description'>Description</th>" +
            "<th class='checkout-model'>SKU</th>" +
            "<th class='checkout-quantity'> Quantity</th >" +
            "<th class='checkout-price'> Unit price</th>" +
            "<th class='checkout-total' colspan='2'> Total</th > " +
            "</tr>");
        for (var i = 0; i < cartQuantity; i++) {
            cartTotalPrice = cartTotalPrice + retrievedCartObject[i].totalPrice;

            $("#cartTable").append("<tr>" +
                "<td class='checkout-image'><a href='javascript:;'><img src='" + retrievedCartObject[i].productImage + "' alt='" + retrievedCartObject[i].productName + "'></a></td>" +
                "<td class='checkout-description'>" +
                "<h3><a href='/Product/SingleProduct?productId=" + parseInt(retrievedCartObject[i].productId) + "'>" + retrievedCartObject[i].productName + "</a></h3>" +
                "<p>Color: " + retrievedCartObject[i].colorName + "; Size: " + retrievedCartObject[i].sizeName + "</p>" +
                "</td>" +
                "<td class='checkout-model'>" + retrievedCartObject[i].sku + "</td>" +
                "<td class='checkout-quantity'>" + retrievedCartObject[i].quantity + "</td>" +
                "<td class='checkout-price'><strong><span>&#2547</span>" + retrievedCartObject[i].price + "</strong></td>" +
                "<td class='checkout-total'><strong><span>&#2547</span>" + retrievedCartObject[i].totalPrice + "</strong>" +
                "<input type='hidden' class='productId' value='" + parseInt(retrievedCartObject[i].productId) + "'>" +
                "</td>" +
                "</tr>");
        }

        var cardNetAmount = parseFloat(cartTotalPrice) - (parseFloat(cartTotalPrice) * (parseFloat(discountPercent) / 100));
        cardNetAmount = cardNetAmount + shippingCost;

        if (discountPercent == 0) {
            $("#cartSummery").append("<ul>" +
                "<li>" +
                "<em>Sub total</em>" +
                "<strong class='price'><span>&#2547</span>" + cartTotalPrice + "</strong>" +
                "</li>" +
                "<li>" +
                "<em> Shipping cost</em>" +
                "<strong class='price'><span>&#2547</span>" + shippingCost + "</strong>" +
                "</li>" +
                "<li class='shopping-total-price'>" +
                "<em>Total</em> " +
                "<strong class='priceTotalCheckout'><span>&#2547</span>" + parseFloat(cartTotalPrice + shippingCost) + "</strong>" +
                "</li>" +
                "</ul>");
        } else {
            $("#cartSummery").append("<ul>" +
                "<li>" +
                "<em>Sub total</em>" +
                "<strong class='price'><span>&#2547</span>" + cartTotalPrice + "</strong>" +
                "</li>" +
                "<li>" +
                "<em> Shipping cost</em>" +
                "<strong class='price'><span>&#2547</span>" + shippingCost + "</strong>" +
                "</li>" +
                "<li>" +
                "<em> Discount</em>" +
                "<strong class='discount'>" + discountPercent + "%</strong>" +
                "</li>" +
                "<li class='shopping-total-price'>" +
                "<em>Total</em> " +
                "<strong class='priceTotalCheckout'><span>&#2547</span>" + cardNetAmount + "</strong>" +
                "</li>" +
                "</ul>");
        }

        
    }
}