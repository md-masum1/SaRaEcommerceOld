﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using SaRaEcommerce.ViewModel;

namespace SaRaEcommerce.Dal
{
    public class WebContentDal
    {
        private OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        #region Return Form Section

        public async Task<List<OrderedProductModel>> GetOrderedProduct(string orderNumber)
        {
            const string sql = "SELECT " +
                                "PRODUCT_ID," +
                                "PRODUCT_NAME," +
                                "SKU," +
                                "COLOR_ID," +
                                "COLOR_NAME," +
                                "SIZE_ID," +
                                "SIZE_NAME," +
                                "QUANTITY," +
                                "PRICE," +
                                "ORDER_NUMBER," +
                                "ORDER_DATE " +
                               "FROM VEW_DELIVERED_PRODUCT WHERE ORDER_NUMBER = :ORDER_NUMBER ";


            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            var objOrderList = new List<OrderedProductModel>();
                            while (await objDataReader.ReadAsync())
                            {
                                OrderedProductModel model = new OrderedProductModel();

                                model.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                model.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                model.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                model.Sku = objDataReader["SKU"].ToString();
                                model.ColorId = Convert.ToInt32(objDataReader["COLOR_ID"].ToString());
                                model.ColorName = objDataReader["COLOR_NAME"].ToString();
                                model.SizeId = Convert.ToInt32(objDataReader["SIZE_ID"].ToString());
                                model.SizeName = objDataReader["SIZE_NAME"].ToString();
                                model.Quantity = Convert.ToInt32(objDataReader["QUANTITY"].ToString());
                                model.Price = Convert.ToDouble(objDataReader["PRICE"].ToString());

                                objOrderList.Add(model);
                            }
                            return objOrderList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<bool> CheckProductExist(string orderNumber)
        {
            var returnMessage = false;
            const string sql = "SELECT " +
                               "ORDER_NUMBER " +
                               "FROM VEW_CUSTOMER_ORDER_STATUS WHERE ORDER_NUMBER = :ORDER_NUMBER AND RETURN_YN = 'N' AND REJECT_YN = 'N' ";


            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                returnMessage = !string.IsNullOrWhiteSpace(objDataReader["ORDER_NUMBER"].ToString());
                            }
                            return returnMessage;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<Tuple<string, string>> SaveReturnRequest(ReturnFormModel returnForm)
        {
            string strMsg;
            string orderNumber;

            OracleCommand objOracleCommand = new OracleCommand("PRO_RETURN_REQUEST_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_RETURN_REASON", OracleDbType.Varchar2, ParameterDirection.Input).Value = returnForm.ReturnReason;
            objOracleCommand.Parameters.Add("P_EXCHANGE_TYPE", OracleDbType.Varchar2, ParameterDirection.Input).Value = returnForm.ExchangeType;
            objOracleCommand.Parameters.Add("P_COMMENTS", OracleDbType.Varchar2, ParameterDirection.Input).Value = returnForm.Comment;
            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = returnForm.OrderNumber;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
            objOracleCommand.Parameters.Add("P_RETURN_REQUEST_ID", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                    orderNumber = objOracleCommand.Parameters["P_RETURN_REQUEST_ID"].Value.ToString();
                }
                catch (Exception ex)
                {
                    _trans.Rollback();
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return new Tuple<string, string>(strMsg, orderNumber);
        }

        public async Task<string> SaveReturnRequestProduct(List<OrderedProductModel> returnedProduct, string returnNumber)
        {
            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand("PRO_RETURN_REQUEST_PRO_SAVE", objConnection) { CommandType = CommandType.StoredProcedure })
                {
                    await objConnection.OpenAsync();

                    try
                    {
                        string vMessage = null;

                        foreach (var data in returnedProduct)
                        {
                            objCommand.Parameters.Clear();

                            objCommand.Parameters.Add("P_RETURN_REQUEST_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(returnNumber) ? returnNumber : null;
                            objCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(data.OrderNumber) ? data.OrderNumber : null;
                            objCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = data.ProductId > 0 ? data.ProductId : (object)null;
                            objCommand.Parameters.Add("P_COLOR_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = data.ColorId > 0 ? data.ColorId : (object)null;
                            objCommand.Parameters.Add("P_SIZE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = data.SizeId > 0 ? data.SizeId : (object)null;
                            objCommand.Parameters.Add("P_QUANTITY ", OracleDbType.Varchar2, ParameterDirection.Input).Value = data.Quantity > 0 ? data.Quantity : (object)null;

                            objCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

                            using (_trans = objConnection.BeginTransaction())
                            {
                                try
                                {
                                    await objCommand.ExecuteNonQueryAsync();
                                    _trans.Commit();

                                    vMessage = objCommand.Parameters["P_MESSAGE"].Value.ToString();
                                }
                                catch (Exception ex)
                                {
                                    _trans.Rollback();
                                    throw new Exception("Error : " + ex.Message);
                                }
                                finally
                                {
                                    _trans.Dispose();
                                }
                            }
                        }
                        return vMessage;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error : " + ex.Message);
                    }
                    finally
                    {
                        objConnection.Close();
                        objCommand.Dispose();
                        objConnection.Dispose();
                    }
                }
            }
        }

        #endregion

    }
}