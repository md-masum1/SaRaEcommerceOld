﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using SaRaEcommerce.ViewModel;

namespace SaRaEcommerce.Dal
{
    public class CustomerDal
    {
        private OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        public async Task<IEnumerable<OrderStatus>> GetCustomerOrderStatus(string customerId)
        {
            const string sql = "SELECT " +
                                "ORDER_NUMBER," +
                                "ORDER_DATE," +
                                "REG_CUSTOMER_ID," +
                                "CUSTOMER_NAME," +
                                "CUSTOMER_EMAIL," +
                                "CUSTOMER_PHONE," +
                                "CUSTOMER_ADDRESS," +
                                "DELEVERY_YN," +
                                "DELEVERY_DATE," +
                                "RETURN_YN," +
                                "RETURN_DATE," +
                                "REJECT_YN," +
                                "REJECT_DATE," +
                                "REJECT_REMARKS," +
                                "PAYMENT_TYPE," +
                                "PAYMENT_STATUS " +
                                "FROM VEW_CUSTOMER_ORDER_STATUS where REG_CUSTOMER_ID = :REG_CUSTOMER_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":REG_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = customerId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<OrderStatus> objOrderModel = new List<OrderStatus>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                OrderStatus model = new OrderStatus();

                                model.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                model.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                model.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                model.CustomerEmail = objDataReader["CUSTOMER_EMAIL"].ToString();
                                model.CustomerPhone = objDataReader["CUSTOMER_PHONE"].ToString();
                                model.CustomerAddress = objDataReader["CUSTOMER_ADDRESS"].ToString();

                                model.DeliveryStatus = objDataReader["DELEVERY_YN"].ToString();
                                model.DeliveryDate = objDataReader["DELEVERY_DATE"].ToString();
                                model.ReturnStatus = objDataReader["RETURN_YN"].ToString();
                                model.ReturnDate = objDataReader["RETURN_DATE"].ToString();

                                model.RejectStatus = objDataReader["REJECT_YN"].ToString();
                                model.RejectDate = objDataReader["REJECT_DATE"].ToString();

                                model.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();

                                if (model.RejectStatus == "Y")
                                {
                                    model.DeliveryStatus = "Canceled";
                                }
                                else
                                {
                                    model.DeliveryStatus = model.DeliveryStatus == "Y" ? "approved" : "processing";
                                }


                                if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
                                    model.PaymentType = "Cash";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "1")
                                    model.PaymentType = "NEXUS Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
                                    model.PaymentType = "DBBL Master Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
                                    model.PaymentType = "DBBL Visa Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
                                    model.PaymentType = "Visa Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
                                    model.PaymentType = "Master Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "6")
                                    model.PaymentType = "Rocket Mobile Banking";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "7")
                                    model.PaymentType = "bKash Mobile Banking";


                                model.ReturnStatus = model.ReturnStatus == "Y" ? "approved" : "processing";

                                objOrderModel.Add(model);
                            }
                            return objOrderModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<OrderStatus> GetCustomerSingleOrder(string customerId, string orderNumber)
        {
            const string sql = "SELECT " +
                                "ORDER_NUMBER," +
                                "ORDER_DATE," +
                                "REG_CUSTOMER_ID," +
                                "CUSTOMER_NAME," +
                                "CUSTOMER_EMAIL," +
                                "CUSTOMER_PHONE," +
                                "CUSTOMER_ADDRESS," +
                                "SHIPPING_COST," +
                                "DISCOUNT_PERCENT," +
                                "DELEVERY_YN," +
                                "DELEVERY_DATE," +
                                "RETURN_YN," +
                                "RETURN_DATE," +
                                "REJECT_YN," +
                                "REJECT_DATE," +
                                "REJECT_REMARKS " +
                                "FROM VEW_CUSTOMER_ORDER_STATUS where REG_CUSTOMER_ID = :REG_CUSTOMER_ID AND ORDER_NUMBER = :ORDER_NUMBER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":REG_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = customerId;
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            OrderStatus model = new OrderStatus();

                            while (await objDataReader.ReadAsync())
                            {
                                model.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                model.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                model.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                model.CustomerEmail = objDataReader["CUSTOMER_EMAIL"].ToString();
                                model.CustomerPhone = objDataReader["CUSTOMER_PHONE"].ToString();
                                model.CustomerAddress = objDataReader["CUSTOMER_ADDRESS"].ToString();

                                model.ShippingCost = Convert.ToDouble(objDataReader["SHIPPING_COST"].ToString());
                                model.DiscountPercentage = objDataReader["DISCOUNT_PERCENT"].ToString();

                                model.DeliveryStatus = objDataReader["DELEVERY_YN"].ToString();
                                model.DeliveryDate = objDataReader["DELEVERY_DATE"].ToString();
                                model.ReturnStatus = objDataReader["RETURN_YN"].ToString();
                                model.ReturnDate = objDataReader["RETURN_DATE"].ToString();

                                model.RejectStatus = objDataReader["REJECT_YN"].ToString();
                                model.RejectDate = objDataReader["REJECT_DATE"].ToString();
                                model.RejectRemarks = objDataReader["REJECT_REMARKS"].ToString();

                                if (model.RejectStatus == "Y")
                                {
                                    model.DeliveryStatus = "Canceled";
                                }
                                else
                                {
                                    model.DeliveryStatus = model.DeliveryStatus == "Y" ? "approved" : "processing";
                                }
                                model.ReturnStatus = model.ReturnStatus == "Y" ? "approved" : "processing";
                            }
                            return model;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<OrderSummery>> GetCustomerOrderSummery(string customerId, string orderNumber)
        {
            const string sql = "SELECT " +
                               "PRODUCT_ID," +
                                "PRODUCT_NAME," +
                                "PRODUCT_IMG," +
                                "SKU," +
                                "COLOR_ID," +
                                "COLOR_NAME," +
                                "SIZE_ID," +
                                "SIZE_NAME," +
                                "QUANTITY," +
                                "PRICE," +
                                "TOTAL_PRICE," +
                                "ORDER_NUMBER," +
                                "ORDER_DATE," +
                                "REG_CUSTOMER_ID " +
                                "FROM VEW_CUSTOMER_ORDER_SUMMARY where REG_CUSTOMER_ID = :REG_CUSTOMER_ID AND ORDER_NUMBER = :ORDER_NUMBER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":REG_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = customerId;
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<OrderSummery> objOrderSummery = new List<OrderSummery>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                OrderSummery model = new OrderSummery();

                                model.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                model.ProductImage = objDataReader["PRODUCT_IMG"].ToString();
                                model.Sku = objDataReader["SKU"].ToString();
                                model.ColorName = objDataReader["COLOR_NAME"].ToString();
                                model.SizeName = objDataReader["SIZE_NAME"].ToString();

                                model.Quantity = Convert.ToInt32(objDataReader["QUANTITY"].ToString());
                                model.Price = Convert.ToDouble(objDataReader["PRICE"].ToString());
                                model.TotalPrice = Convert.ToDouble(objDataReader["TOTAL_PRICE"].ToString());

                                objOrderSummery.Add(model);
                            }
                            return objOrderSummery;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> OrderedProductDelete(string orderNumber, string customerId)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_CUSTOMER_ORDER_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;
            objOracleCommand.Parameters.Add("P_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = customerId;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }
    }
}