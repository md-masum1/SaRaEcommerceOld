﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using SaRaEcommerce.ViewModel;

namespace SaRaEcommerce.Dal
{
    public class HomeDal
    {
        private OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        #region Home Page Slider And Banner

        public async Task<IEnumerable<HomepageSlider>> GetHomepageSlider()
        {
            const string sql = "SELECT " +
                               "SLIDER_ID," +
                               "SLIDER_TITLE," +
                               "SLIDER_DESCRIPTION," +
                               "SLIDER_URL," +
                               "DISPLAY_ORDER," +
                               "SLIDER_IMAGE " +
                               "FROM VEW_HOMEPAGE_SLIDER ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<HomepageSlider> objSliderModel = new List<HomepageSlider>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                HomepageSlider slider = new HomepageSlider();

                                slider.SliderId = Convert.ToInt32(objDataReader["SLIDER_ID"].ToString());
                                slider.SliderTitle = objDataReader["SLIDER_TITLE"].ToString();
                                slider.SliderDescription = objDataReader["SLIDER_DESCRIPTION"].ToString();

                                slider.SliderUrl = objDataReader["SLIDER_URL"].ToString();
                                slider.SliderImage = objDataReader["SLIDER_IMAGE"].ToString();

                                objSliderModel.Add(slider);
                            }
                            return objSliderModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<HomepageBanner>> GetHomepageBanner()
        {
            const string sql = "SELECT " +
                               "BANNER_ID," +
                               "BANNER_TITLE," +
                               "BANNER_DESCRIPTION," +
                               "BANNER_URL," +
                               "DISPLAY_ORDER," +
                               "BANNER_IMAGE " +
                               "FROM VEW_HOMEPAGE_BANNER ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<HomepageBanner> objBannerModel = new List<HomepageBanner>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                HomepageBanner banner = new HomepageBanner();

                                banner.BannerId = Convert.ToInt32(objDataReader["BANNER_ID"].ToString());
                                banner.BannerTitle = objDataReader["BANNER_TITLE"].ToString();
                                banner.BannerDescription = objDataReader["BANNER_DESCRIPTION"].ToString();
                                banner.BannerUrl = objDataReader["BANNER_URL"].ToString();
                                banner.BannerImage = objDataReader["BANNER_IMAGE"].ToString();

                                objBannerModel.Add(banner);
                            }
                            return objBannerModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<HomepageOffer>> GetHomepageOffers()
        {
            const string sql =  "SELECT " +
                                "OFFER_ID," +
                                "OFFER_URL," +
                                "OFFER_IMAGE," +
                                "DISPLAY_ORDER " +
                                "FROM VEW_HOMEPAGE_OFFER ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<HomepageOffer> objOfferModel = new List<HomepageOffer>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                HomepageOffer offer = new HomepageOffer();

                                offer.OfferId = Convert.ToInt32(objDataReader["OFFER_ID"].ToString());

                                offer.OfferUrl = objDataReader["OFFER_URL"].ToString();
                                offer.OfferImage = objDataReader["OFFER_IMAGE"].ToString();

                                objOfferModel.Add(offer);
                            }
                            return objOfferModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> GetHomePagePopup()
        {
            const string sql = "SELECT POPUP_IMAGE FROM VEW_HOMEPAGE_POPUP WHERE ACTIVE_YN = 'Y' ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            string imageName = null;
                            while (await objDataReader.ReadAsync())
                            {
                                imageName = objDataReader["POPUP_IMAGE"].ToString();
                            }
                            return imageName;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Menu Section

        public async Task<IEnumerable<MenuMain>> GetMenuMain()
        {
            var sql = "SELECT " +
                        "CATEGORY_ID," +
                        "CATEGORY_NAME," +
                        "DISPLAY_ORDER " +
                        "FROM VEW_CATEGORY ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<MenuMain> objMenuModel = new List<MenuMain>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                MenuMain menuMain = new MenuMain();

                                menuMain.MenuMainId = Convert.ToInt32(objDataReader["CATEGORY_ID"].ToString());
                                menuMain.MenuMainName = objDataReader["CATEGORY_NAME"].ToString();

                                objMenuModel.Add(menuMain);
                            }
                            return objMenuModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<MenuSub>> GetMenuSub(int menuId)
        {
            var sql = "SELECT " +
                      "CATEGORY_ID," +
                      "SUB_CATEGORY_ID," +
                      "SUB_CATEGORY_NAME," +
                      "DISPLAY_ORDER " +
                      "FROM VEW_SUB_CATEGORY WHERE CATEGORY_ID = :CATEGORY_ID ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = menuId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<MenuSub> objMenuModel = new List<MenuSub>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                MenuSub menuSub = new MenuSub();

                                menuSub.MenuMainId = Convert.ToInt32(objDataReader["CATEGORY_ID"].ToString());
                                menuSub.MenuSubId = Convert.ToInt32(objDataReader["SUB_CATEGORY_ID"].ToString());
                                menuSub.MenuSubName = objDataReader["SUB_CATEGORY_NAME"].ToString();

                                objMenuModel.Add(menuSub);
                            }
                            return objMenuModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<MenuSubSub>> GetMenuSubSub(int menuId, int menuSubId)
        {
            var sql = "SELECT " +
                      "CATEGORY_ID," +
                      "SUB_CATEGORY_ID," +
                      "SUB_SUB_CATEGORY_ID," +
                      "SUB_SUB_CATEGORY_NAME," +
                      "DISPLAY_ORDER " +
                      "FROM VEW_SUB_SUB_CATEGORY WHERE CATEGORY_ID = :CATEGORY_ID AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = menuId;
                    objCommand.Parameters.Add(":SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = menuSubId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<MenuSubSub> objMenuModel = new List<MenuSubSub>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                MenuSubSub menuSubSub = new MenuSubSub();

                                menuSubSub.MenuMainId = Convert.ToInt32(objDataReader["CATEGORY_ID"].ToString());
                                menuSubSub.MenuSubId = Convert.ToInt32(objDataReader["SUB_CATEGORY_ID"].ToString());
                                menuSubSub.MenuSubSubId = Convert.ToInt32(objDataReader["SUB_SUB_CATEGORY_ID"].ToString());
                                menuSubSub.MenuSubSubName = objDataReader["SUB_SUB_CATEGORY_NAME"].ToString();

                                objMenuModel.Add(menuSubSub);
                            }
                            return objMenuModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Home Page Product Display Section

        public async Task<IEnumerable<ProductDisplaySection>> GetUpcomingProduct()
        {
            var sql = "SELECT " +
                      "UPCOMING_PRODUCT_ID," +
                      "PRODUCT_NAME," +
                      "SKU," +
                      "PRICE," +
                      "IMAGE " +
                      "FROM VEW_UPCOMING_PRODUCT ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductDisplaySection> upcomingProduct = new List<ProductDisplaySection>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductDisplaySection product = new ProductDisplaySection();

                                product.ProductId = Convert.ToInt32(objDataReader["UPCOMING_PRODUCT_ID"].ToString());
                                product.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                product.Sku = objDataReader["SKU"].ToString();
                                product.NewPrice = objDataReader["PRICE"].ToString();
                                product.ProductImage = objDataReader["IMAGE"].ToString();

                                upcomingProduct.Add(product);
                            }
                            return upcomingProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> PlacePreOrder(int productId, string customerId)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_PRE_ORDER_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
            objOracleCommand.Parameters.Add("P_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = customerId;


            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<IEnumerable<ProductDisplaySection>> GetOccasionalProducts()
        {
            var sql = "SELECT " +
                      "PRODUCT_ID," +
                      "PRODUCT_NAME," +
                      "SKU," +
                      "PRIMARY_IMAGE," +
                      "PRICE " +
                      "FROM VEW_OCCASIONAL_PRODUCT ORDER BY OCCASIONAL_PRODUCT_ID DESC ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductDisplaySection> occasionalProduct = new List<ProductDisplaySection>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductDisplaySection product = new ProductDisplaySection();

                                product.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                product.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                product.Sku = objDataReader["SKU"].ToString();
                                product.ProductImage = objDataReader["PRIMARY_IMAGE"].ToString();
                                product.NewPrice = objDataReader["PRICE"].ToString();


                                occasionalProduct.Add(product);
                            }
                            return occasionalProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<HomepageSlider>> GetOccasionalSlider()
        {
            const string sql = "SELECT " +
                               "SLIDER_ID," +
                               "SLIDER_TITLE," +
                               "SLIDER_URL," +
                               "DISPLAY_ORDER," +
                               "SLIDER_IMAGE " +
                               "FROM VEW_OCCASIONAL_SLIDER ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<HomepageSlider> objSliderModel = new List<HomepageSlider>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                HomepageSlider slider = new HomepageSlider();

                                slider.SliderId = Convert.ToInt32(objDataReader["SLIDER_ID"].ToString());
                                slider.SliderTitle = objDataReader["SLIDER_TITLE"].ToString();

                                slider.SliderUrl = objDataReader["SLIDER_URL"].ToString();
                                slider.SliderImage = objDataReader["SLIDER_IMAGE"].ToString();

                                objSliderModel.Add(slider);
                            }
                            return objSliderModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<ProductDisplaySection>> GetNewArrivalsProduct()
        {
            var sql = "SELECT " +
                      "PRODUCT_ID," +
                      "PRODUCT_NAME," +
                      "SKU," +
                      "DISCOUNT," +
                      "PRICE," +
                      "NEW_PRICE," +
                      "PRIMARY_IMAGE " +
                      "FROM VEW_NEW_ARRIVAL_PRODUCT ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductDisplaySection> newArrivalsProduct = new List<ProductDisplaySection>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductDisplaySection product = new ProductDisplaySection();

                                product.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                product.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                product.Sku = objDataReader["SKU"].ToString();
                                product.ProductImage = objDataReader["PRIMARY_IMAGE"].ToString();
                                product.DiscountPercentage = objDataReader["DISCOUNT"].ToString();
                                product.PreviousPrice = objDataReader["PRICE"].ToString();
                                product.NewPrice = objDataReader["NEW_PRICE"].ToString();


                                newArrivalsProduct.Add(product);
                            }
                            return newArrivalsProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<ProductDisplaySection>> GetTrendingProducts()
        {
            var sql = "SELECT " +
                      "PRODUCT_ID," +
                      "PRODUCT_NAME," +
                      "SKU," +
                      "DISCOUNT," +
                      "PRIMARY_IMAGE," +
                      "PRICE," +
                      "NEW_PRICE " +
                      "FROM VEW_TRENDING_PRODUCT ORDER BY TRENDING_PRODUCT_ID DESC ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductDisplaySection> trendingProduct = new List<ProductDisplaySection>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductDisplaySection product = new ProductDisplaySection();

                                product.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                product.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                product.Sku = objDataReader["SKU"].ToString();
                                product.ProductImage = objDataReader["PRIMARY_IMAGE"].ToString();
                                product.DiscountPercentage = objDataReader["DISCOUNT"].ToString();
                                product.PreviousPrice = objDataReader["PRICE"].ToString();
                                product.NewPrice = objDataReader["NEW_PRICE"].ToString();


                                trendingProduct.Add(product);
                            }
                            return trendingProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<ProductDisplaySection>> GetPromotionalProducts()
        {
            var sql = "SELECT " +
                      "PRODUCT_ID," +
                      "PRODUCT_NAME," +
                      "SKU," +
                      "IMAGE," +
                      "PREVIOUS_PRICE," +
                      "NEW_PRICE," +
                      "PROMOTION_PERCENTAGE " +
                      "FROM VEW_PROMOTIONAL_PRODUCTS WHERE STATUS = 'ACTIVE' AND ROWNUM <=20";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductDisplaySection> promotionalProducts = new List<ProductDisplaySection>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductDisplaySection product = new ProductDisplaySection();

                                product.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                product.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                product.Sku = objDataReader["SKU"].ToString();
                                product.ProductImage = objDataReader["IMAGE"].ToString();
                                product.DiscountPercentage = objDataReader["PROMOTION_PERCENTAGE"].ToString();
                                product.PreviousPrice = objDataReader["PREVIOUS_PRICE"].ToString();
                                product.NewPrice = objDataReader["NEW_PRICE"].ToString();


                                promotionalProducts.Add(product);
                            }
                            return promotionalProducts;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Contact
        public async Task<string> SaveCustomerMessage(CustomerContact objContact)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_CUSTOMER_CONTACT_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_CONTACT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;

            objOracleCommand.Parameters.Add("P_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objContact.CustomerId) ? objContact.CustomerId : null;

            objOracleCommand.Parameters.Add("P_CUSTOMER_NAME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objContact.CustomerName) ? objContact.CustomerName : null;
            objOracleCommand.Parameters.Add("P_CUSTOMER_EMAIL", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objContact.CustomerEmail) ? objContact.CustomerEmail : null;
            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.NVarchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objContact.Message) ? objContact.Message : null;
            objOracleCommand.Parameters.Add("P_MACHINE_IP", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objContact.IpOrMac) ? objContact.IpOrMac : null;


            objOracleCommand.Parameters.Add("P_out_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_out_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }
        #endregion


    }
}