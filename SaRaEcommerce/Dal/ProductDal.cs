﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using SaRaEcommerce.ViewModel;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace SaRaEcommerce.Dal
{
    public class ProductDal
    {
        private OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        #region Product List Display

        public async Task<IEnumerable<ProductModel>> GetProductList(int? categoryId, int? subCategoryId, int? subSubCategoryId, string pageLength, string shortBy, int? currentPage, int? take, int? skip, int?[] productId, double? minPrice, double? maxPrice)
        {
            string sql = "SELECT " +
                         "ROWNUM RN, " +
                               "PRODUCT_ID," +
                                "PRODUCT_NAME," +
                                "SKU," +
                                "PRIMARY_IMAGE," +
                                "SELL_AMOUNT," +
                                "DISCOUNT," +
                                "NEW_PRICE," +
                                "TOTALQUANTITY," +
                                "CREATE_DATE ";

            if (shortBy != null && shortBy == "0")
                sql += "FROM VEW_UI_PRODUCT_ID_DESC WHERE 1=1 ";
            else if (shortBy != null && shortBy == "1")
                sql += "FROM VEW_UI_PRODUCT_NAME_ASC WHERE 1=1 ";
            else if (shortBy != null && shortBy == "2")
                sql += "FROM VEW_UI_PRODUCT_NAME_DESC WHERE 1=1 ";
            else if (shortBy != null && shortBy == "3")
                sql += "FROM VEW_UI_PRODUCT_PRICE_ASC WHERE 1=1 ";
            else if (shortBy != null && shortBy == "4")
                sql += "FROM VEW_UI_PRODUCT_PRICE_DESC WHERE 1=1 ";
            else if (shortBy != null && shortBy == "5")
                sql += "FROM VEW_UI_PRODUCT_ID_ASC WHERE 1=1 ";
            else if (shortBy != null && shortBy == "6")
                sql += "FROM VEW_UI_PRODUCT_ID_DESC WHERE 1=1 ";
            else
                sql += "FROM VEW_UI_PRODUCT_INFO WHERE 1=1 ";

            if (minPrice != null && maxPrice != null && maxPrice > minPrice)
            {
                sql += "AND SELL_AMOUNT BETWEEN "+ minPrice +" AND "+ maxPrice +" ";
            }

            if (categoryId != null && categoryId != 0 && subCategoryId == null && subSubCategoryId == null)
                sql += "AND CATEGORY_ID = :CATEGORY_ID ";

            if (categoryId != null && categoryId != 0 && subCategoryId != null && subCategoryId != 0 &&
                subSubCategoryId == null)
                sql += "AND CATEGORY_ID = :CATEGORY_ID AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID ";

            if (categoryId != null && categoryId != 0 && subCategoryId != null && subCategoryId != 0 &&
                subSubCategoryId != null && subSubCategoryId != 0)
                sql += "AND CATEGORY_ID = :CATEGORY_ID AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID AND SUB_SUB_CATEGORY_ID = :SUB_SUB_CATEGORY_ID ";

            if (productId != null)
            {
                string productByColor = string.Join(",", productId);
                sql += "AND PRODUCT_ID IN (" + productByColor + ")";
            }

            if (take != null && skip != null)
            {
                sql = "SELECT * FROM (" + sql + ") WHERE RN BETWEEN  '" + take + "' AND '" + skip + "' ";
            }
            else
            {
                if (pageLength != null && pageLength == "16")
                    sql += "AND ROWNUM <= 16 ";
                else if (pageLength != null && pageLength == "32")
                    sql += "AND ROWNUM <= 32 ";
                else if (pageLength != null && pageLength == "48")
                    sql += "AND ROWNUM <= 48 ";
                else if (pageLength != null && pageLength == "64")
                    sql += "AND ROWNUM <= 64 ";
                else if (pageLength != null && pageLength == "100")
                    sql += "AND ROWNUM <= 100 ";
                else
                    sql += "AND ROWNUM <= 16 ";
            }




            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    if (categoryId != null && categoryId != 0)
                        objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;
                    if (subCategoryId != null && subCategoryId != 0)
                        objCommand.Parameters.Add(":SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;
                    if (subSubCategoryId != null && subSubCategoryId != 0)
                        objCommand.Parameters.Add(":SUB_SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subSubCategoryId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductModel> objProductModel = new List<ProductModel>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductModel product = new ProductModel();

                                product.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                product.ProductName = objDataReader["PRODUCT_NAME"].ToString();

                                product.Sku = objDataReader["SKU"].ToString();

                                product.PrimaryImage = objDataReader["PRIMARY_IMAGE"].ToString();

                                product.InsertDate = objDataReader["CREATE_DATE"].ToString();

                                product.SellAmount = Convert.ToDouble(objDataReader["SELL_AMOUNT"]);
                                product.Discount = Convert.ToInt32(objDataReader["DISCOUNT"]);
                                product.NewAmount = Convert.ToDouble(objDataReader["NEW_PRICE"]);

                                product.TotalQuantity = Convert.ToDouble(objDataReader["TOTALQUANTITY"]);

                                objProductModel.Add(product);
                            }
                            return objProductModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region SingleProduct Desplay

        public async Task<SingleProduct> GetSingleProduct(int productId)
        {
            string sql = "SELECT " +
                        "SL," +
                        "PRODUCT_ID," +
                        "PRODUCT_NAME," +
                        "SKU," +
                        "FABRIC_NAME," +
                        "SHORT_DESCRIPTION," +
                        "LONG_DESCRIPTION," +
                        "CATEGORY_ID," +
                        "SUB_CATEGORY_ID," +
                        "SUB_SUB_CATEGORY_ID," +
                        "CATEGORY_NAME," +
                        "SUB_CATEGORY_NAME," +
                        "SUB_SUB_CATEGORY_NAME," +
                        "PRIMARY_IMAGE," +
                        "SELL_AMOUNT," +
                        "DISCOUNT," +
                        "NEW_PRICE," +
                        "TOTALQUANTITY," +
                        "REVIEW_SCORE," +
                        "TOTAL_REVIEW_COUNT," +
                        "CREATE_DATE " +
                        "FROM VEW_UI_PRODUCT_INFO WHERE PRODUCT_ID = :PRODUCT_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        SingleProduct objProductModel = new SingleProduct();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objProductModel.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                objProductModel.ProductName = objDataReader["PRODUCT_NAME"].ToString();

                                objProductModel.Sku = objDataReader["SKU"].ToString();
                                objProductModel.FabricName = objDataReader["FABRIC_NAME"].ToString();

                                objProductModel.CategoryId = Convert.ToInt32(objDataReader["CATEGORY_ID"]);
                                objProductModel.SubCategoryId = Convert.ToInt32(objDataReader["SUB_CATEGORY_ID"]);
                                objProductModel.SubSubCategoryId = Convert.ToInt32(objDataReader["SUB_SUB_CATEGORY_ID"]);

                                objProductModel.CategoryName = objDataReader["CATEGORY_NAME"].ToString();
                                objProductModel.SubCategoryName = objDataReader["SUB_CATEGORY_NAME"].ToString();
                                objProductModel.SubSubCategoryName = objDataReader["SUB_SUB_CATEGORY_NAME"].ToString();

                                objProductModel.PrimaryImage = objDataReader["PRIMARY_IMAGE"].ToString();

                                objProductModel.ShortDescription = objDataReader["SHORT_DESCRIPTION"].ToString();
                                objProductModel.LongDescription = objDataReader["LONG_DESCRIPTION"].ToString();

                                objProductModel.InsertDate = objDataReader["CREATE_DATE"].ToString();

                                objProductModel.SellAmount = Convert.ToDouble(objDataReader["SELL_AMOUNT"]);
                                objProductModel.Discount = Convert.ToDouble(objDataReader["DISCOUNT"]);
                                objProductModel.NewAmount = Convert.ToDouble(objDataReader["NEW_PRICE"]);

                                objProductModel.TotalQuantity = Convert.ToDouble(objDataReader["TOTALQUANTITY"]);

                                if (!string.IsNullOrWhiteSpace(objDataReader["REVIEW_SCORE"].ToString()))
                                    objProductModel.ReviewScore = Convert.ToDouble(objDataReader["REVIEW_SCORE"]);
                                else
                                    objProductModel.ReviewScore = 0;

                                objProductModel.TotalReviewCount = Convert.ToInt32(objDataReader["TOTAL_REVIEW_COUNT"].ToString());
                            }
                            return objProductModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<RatioList>> GetRatioLists(int productId)
        {
            const string sql = "SELECT DISTINCT " +
                               "PRODUCT_ID, " +
                               "RATIO_ID, " +
                               "RATIO_NAME " +
                               "FROM VEW_PRODUCT_RATIO WHERE PRODUCT_ID = :PRODUCT_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<RatioList> ratioLists = new List<RatioList>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                RatioList model = new RatioList
                                {
                                    RatioId = Convert.ToInt32(objDataReader["RATIO_ID"].ToString()),
                                    RatioName = objDataReader["RATIO_NAME"].ToString()
                                };
                                ratioLists.Add(model);
                            }
                            return ratioLists;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<RatioSizeList>> GetRatioSizeLists(int productId, int ratioId)
        {
            const string sql = "SELECT " +
                               "SIZE_ID," +
                               "SIZE_NAME," +
                               "SIZE_VALUE " +
                               "FROM VEW_PRODUCT_RATIO_SIZE_VALUE WHERE PRODUCT_ID = :PRODUCT_ID AND RATIO_ID = :RATIO_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
                    objCommand.Parameters.Add(":RATIO_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = ratioId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<RatioSizeList> sizeList = new List<RatioSizeList>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                RatioSizeList model = new RatioSizeList
                                {
                                    SizeId = Convert.ToInt32(objDataReader["SIZE_ID"].ToString()),
                                    SizeName = objDataReader["SIZE_NAME"].ToString(),
                                    SizeValue = (float)Convert.ToDouble(objDataReader["SIZE_VALUE"].ToString())
                                };
                                sizeList.Add(model);
                            }
                            return sizeList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Quick View Product Display

        public async Task<ProductForQuickView> GetProductForQuickView(int productId)
        {
            string sql = "SELECT " +
                         "PRODUCT_ID," +
                         "PRODUCT_NAME," +
                         "SKU," +
                         "PRIMARY_IMAGE," +
                         "SHORT_DESCRIPTION," +
                         "SELL_AMOUNT," +
                         "DISCOUNT," +
                         "NEW_PRICE," +
                         "TOTALQUANTITY," +
                         "CREATE_DATE " +
                         "FROM VEW_UI_PRODUCT_INFO WHERE PRODUCT_ID = :PRODUCT_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        ProductForQuickView objProductModel = new ProductForQuickView();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objProductModel.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                objProductModel.ProductName = objDataReader["PRODUCT_NAME"].ToString();

                                objProductModel.Sku = objDataReader["SKU"].ToString();

                                objProductModel.PrimaryImage = objDataReader["PRIMARY_IMAGE"].ToString();

                                objProductModel.ShortDescription = objDataReader["SHORT_DESCRIPTION"].ToString();

                                objProductModel.InsertDate = objDataReader["CREATE_DATE"].ToString();

                                objProductModel.SellAmount = Convert.ToDouble(objDataReader["SELL_AMOUNT"]);
                                objProductModel.Discount = Convert.ToDouble(objDataReader["DISCOUNT"]);
                                objProductModel.NewAmount = Convert.ToDouble(objDataReader["NEW_PRICE"]);

                                objProductModel.TotalQuantity = Convert.ToDouble(objDataReader["TOTALQUANTITY"]);
                            }
                            return objProductModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Related Product Display

        public async Task<IEnumerable<ProductModel>> GetRelatedProduct(int? categoryId, int? subCategoryId, int? subSubCategoryId)
        {
            string sql = "SELECT " +
                        "PRODUCT_ID," +
                        "PRODUCT_NAME," +
                        "SKU," +
                        "CATEGORY_ID," +
                        "SUB_CATEGORY_ID," +
                        "SUB_SUB_CATEGORY_ID," +
                        "PRIMARY_IMAGE," +
                        "SELL_AMOUNT," +
                        "DISCOUNT," +
                        "NEW_PRICE," +
                        "CREATE_DATE " +
                        "FROM VEW_UI_PRODUCT_INFO WHERE 1=1 ";

            if (categoryId != null && categoryId != 0)
            {
                sql += "AND CATEGORY_ID = :CATEGORY_ID ";

                if (subCategoryId != null && subCategoryId != 0)
                {
                    sql += "AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID ";

                    if (subSubCategoryId != null && subSubCategoryId != 0)
                    {
                        sql += "AND SUB_SUB_CATEGORY_ID = :SUB_SUB_CATEGORY_ID ";
                    }
                }
            }
            sql += "AND ROWNUM <= 15 ";


            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    if (categoryId != null && categoryId != 0)
                    {
                        objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;

                        if (subCategoryId != null && subCategoryId != 0)
                        {
                            objCommand.Parameters.Add(":SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;

                            if (subSubCategoryId != null && subSubCategoryId != 0)
                            {
                                objCommand.Parameters.Add(":SUB_SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subSubCategoryId;
                            }
                        }
                    }



                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductModel> objProductModel = new List<ProductModel>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductModel model = new ProductModel();

                                model.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                model.ProductName = objDataReader["PRODUCT_NAME"].ToString();

                                model.Sku = objDataReader["SKU"].ToString();

                                model.PrimaryImage = objDataReader["PRIMARY_IMAGE"].ToString();

                                model.InsertDate = objDataReader["CREATE_DATE"].ToString();

                                model.SellAmount = Convert.ToDouble(objDataReader["SELL_AMOUNT"]);
                                model.Discount = Convert.ToInt32(objDataReader["DISCOUNT"]);
                                model.NewAmount = Convert.ToDouble(objDataReader["NEW_PRICE"]);

                                objProductModel.Add(model);
                            }
                            return objProductModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Common Data For all Product

        public async Task<int> GetInventoryValue(int productId, int colorId, int sizeId)
        {
            int quantity = 0;

            string sql = "select NVL(sum(QUANTITY), 0) total_inventory from VEW_PRODUCT_INVENTORY where PRODUCT_ID = :PRODUCT_ID AND COLOR_ID = :COLOR_ID AND SIZE_ID = :SIZE_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
                    objCommand.Parameters.Add(":COLOR_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = colorId;
                    objCommand.Parameters.Add(":SIZE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = sizeId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                quantity = quantity + Convert.ToInt32(objDataReader["total_inventory"]);
                            }
                            return quantity;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<double> GetProductPrice(int productId, int colorId, int sizeId)
        {
            double price = 0;

            string sql = "select OLD_PRICE from VEW_PRODUCT_INVENTORY where PRODUCT_ID = :PRODUCT_ID AND COLOR_ID = :COLOR_ID AND SIZE_ID = :SIZE_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
                    objCommand.Parameters.Add(":COLOR_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = colorId;
                    objCommand.Parameters.Add(":SIZE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = sizeId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                price = Convert.ToDouble(objDataReader["OLD_PRICE"]);
                            }
                            return price;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<ProductColor>> GetProductColorList(int productId)
        {
            const string sql = "SELECT " +
                               "PRODUCT_ID," +
                                "COLOR_ID," +
                                "COLOR_NAME," +
                                "COLOR_CODE " +
                               "FROM VEW_PRODUCT_IMAGE WHERE PRODUCT_ID = :PRODUCT_ID ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductColor> objProductColor = new List<ProductColor>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductColor color = new ProductColor();

                                color.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                color.ProductColorId = Convert.ToInt32(objDataReader["COLOR_ID"].ToString());
                                color.ProductColorName = objDataReader["COLOR_NAME"].ToString();
                                color.ProductColorCode = objDataReader["COLOR_CODE"].ToString();

                                objProductColor.Add(color);
                            }
                            return objProductColor;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<ProductColor> GetProductImageList(int productId, int colorId)
        {
            const string sql = "SELECT " +
                               "PRODUCT_ID," +
                                "COLOR_ID," +
                                "COLOR_NAME," +
                                "IMAGE_1," +
                                "IMAGE_2," +
                                "IMAGE_3," +
                                "IMAGE_4 " +
                               "FROM VEW_PRODUCT_IMAGE WHERE PRODUCT_ID = :PRODUCT_ID AND COLOR_ID = :COLOR_ID ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
                    objCommand.Parameters.Add(":COLOR_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = colorId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        ProductColor objProductColor = new ProductColor();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objProductColor.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                objProductColor.ProductColorId = Convert.ToInt32(objDataReader["COLOR_ID"].ToString());
                                objProductColor.ProductColorName = objDataReader["COLOR_NAME"].ToString();

                                objProductColor.Image1 = objDataReader["IMAGE_1"].ToString();
                                objProductColor.Image2 = objDataReader["IMAGE_2"].ToString();
                                objProductColor.Image3 = objDataReader["IMAGE_3"].ToString();
                                objProductColor.Image4 = objDataReader["IMAGE_4"].ToString();
                            }
                            return objProductColor;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<ProductSize>> GetProductSizeList(int productId)
        {
            const string sql = "SELECT " +
                               "PRODUCT_ID," +
                                "SIZE_ID," +
                                "SIZE_NAME " +
                               "FROM VEW_PRODUCT_SIZE WHERE PRODUCT_ID = :PRODUCT_ID ORDER BY SIZE_NAME ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductSize> objProductSize = new List<ProductSize>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductSize size = new ProductSize();

                                size.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                size.SizeId = Convert.ToInt32(objDataReader["SIZE_ID"].ToString());
                                size.SizeName = objDataReader["SIZE_NAME"].ToString();

                                objProductSize.Add(size);
                            }
                            return objProductSize;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<int> GetTotalProductCount(int? categoryId, int? subCategoryId, int? subSubCategoryId)
        {
            int totalProduct = 0;

            string sql = "SELECT COUNT (*) TOTAL_PRODUCT FROM VEW_UI_PRODUCT_INFO WHERE 1=1 ";

            if (categoryId != null && categoryId != 0 && subCategoryId == null && subSubCategoryId == null)
                sql += "AND CATEGORY_ID = :CATEGORY_ID ";

            if (categoryId != null && categoryId != 0 && subCategoryId != null && subCategoryId != 0 &&
                subSubCategoryId == null)
                sql += "AND CATEGORY_ID = :CATEGORY_ID AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID ";

            if (categoryId != null && categoryId != 0 && subCategoryId != null && subCategoryId != 0 &&
                subSubCategoryId != null && subSubCategoryId != 0)
                sql += "AND CATEGORY_ID = :CATEGORY_ID AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID AND SUB_SUB_CATEGORY_ID = :SUB_SUB_CATEGORY_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    if (categoryId != null && categoryId != 0)
                        objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;
                    if (subCategoryId != null && subCategoryId != 0)
                        objCommand.Parameters.Add(":SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;
                    if (subSubCategoryId != null && subSubCategoryId != 0)
                        objCommand.Parameters.Add(":SUB_SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subSubCategoryId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                totalProduct = Convert.ToInt32(objDataReader["TOTAL_PRODUCT"]);
                            }
                            return totalProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }
        public async Task<string> GetCategoryName(int categoryId)
        {
            string categoryName = "";
            const string sql = "SELECT " +
                               "CATEGORY_ID," +
                                "CATEGORY_NAME " +
                               "FROM L_CATEGORY WHERE CATEGORY_ID = :CATEGORY_ID ORDER BY CATEGORY_NAME ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                categoryName = objDataReader["CATEGORY_NAME"].ToString();
                            }
                            return categoryName;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }
        public async Task<string> GetSubCategoryName(int categoryId, int subCategoryId)
        {
            string subCategoryName = "";
            const string sql = "SELECT " +
                               "CATEGORY_ID," +
                               "SUB_CATEGORY_ID," +
                               "SUB_CATEGORY_NAME " +
                               "FROM L_SUB_CATEGORY WHERE CATEGORY_ID = :CATEGORY_ID AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID ORDER BY SUB_CATEGORY_NAME ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;
                    objCommand.Parameters.Add(":SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                subCategoryName = objDataReader["SUB_CATEGORY_NAME"].ToString();
                            }
                            return subCategoryName;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }
        public async Task<string> GetSubSubCategoryName(int categoryId, int subCategoryId, int subSubCategoryId)
        {
            string subCategoryName = "";
            const string sql = "SELECT " +
                               "CATEGORY_ID," +
                               "SUB_CATEGORY_ID," +
                               "SUB_SUB_CATEGORY_ID," +
                               "SUB_SUB_CATEGORY_NAME " +
                               "FROM L_SUB_SUB_CATEGORY WHERE CATEGORY_ID = :CATEGORY_ID AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID AND SUB_SUB_CATEGORY_ID = :SUB_SUB_CATEGORY_ID ORDER BY SUB_SUB_CATEGORY_NAME ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;
                    objCommand.Parameters.Add(":SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;
                    objCommand.Parameters.Add(":SUB_SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subSubCategoryId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                subCategoryName = objDataReader["SUB_SUB_CATEGORY_NAME"].ToString();
                            }
                            return subCategoryName;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion


        #region Filter Section      

        public async Task<IEnumerable<ColorForFilter>> ColorFilterList(int? categoryId, int? subCategoryId, int? subSubCategoryId, string[] colorId)
        {
            var sql = "SELECT DISTINCT " +
                      "COLOR_ID," +
                      "COLOR_NAME " +
                      "FROM VEW_FILTER_COLOR_AND_SIZE WHERE 1=1 ";

            if (categoryId != null && categoryId != 0)
            {
                sql += "AND CATEGORY_ID = :CATEGORY_ID ";

                if (subCategoryId != null && subCategoryId != 0)
                {
                    sql += "AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID ";

                    if (subSubCategoryId != null && subSubCategoryId != 0)
                    {
                        sql += "AND SUB_SUB_CATEGORY_ID = :SUB_SUB_CATEGORY_ID ";
                    }
                }
            }

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    if(categoryId != null && categoryId != 0)
                    {
                        objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;

                        if(subCategoryId != null && subCategoryId !=0)
                        {
                            objCommand.Parameters.Add(":SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;

                            if (subSubCategoryId != null && subSubCategoryId != 0)
                            {
                                objCommand.Parameters.Add(":SUB_SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subSubCategoryId;
                            }
                        }
                    }
                    

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ColorForFilter> objColor = new List<ColorForFilter>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ColorForFilter model = new ColorForFilter();

                                model.ColorId = Convert.ToInt32(objDataReader["COLOR_ID"].ToString());
                                model.ColorName = objDataReader["COLOR_NAME"].ToString();

                                if (colorId != null)
                                {
                                    foreach (var color in colorId)
                                    {
                                        if (!string.IsNullOrWhiteSpace(color) && objDataReader["COLOR_ID"].ToString() == color)
                                        {
                                            model.Checked = true;
                                        }
                                    }
                                }
                               
                                objColor.Add(model);
                            }
                            return objColor;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<SizeForFilter>> SizeFilterList(int? categoryId, int? subCategoryId, int? subSubCategoryId, string[] sizeId)
        {
            var sql = "SELECT DISTINCT " +
                      "SIZE_ID," +
                      "SIZE_NAME " +
                      "FROM VEW_FILTER_COLOR_AND_SIZE WHERE 1=1 ";

            if (categoryId != null && categoryId != 0)
            {
                sql += "AND CATEGORY_ID = :CATEGORY_ID ";

                if (subCategoryId != null && subCategoryId != 0)
                {
                    sql += "AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID ";

                    if (subSubCategoryId != null && subSubCategoryId != 0)
                    {
                        sql += "AND SUB_SUB_CATEGORY_ID = :SUB_SUB_CATEGORY_ID ";
                    }
                }
            }

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    if (categoryId != null && categoryId != 0)
                    {
                        objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;

                        if (subCategoryId != null && subCategoryId != 0)
                        {
                            objCommand.Parameters.Add(":SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;

                            if (subSubCategoryId != null && subSubCategoryId != 0)
                            {
                                objCommand.Parameters.Add(":SUB_SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subSubCategoryId;
                            }
                        }
                    }


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<SizeForFilter> objSize = new List<SizeForFilter>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                SizeForFilter model = new SizeForFilter();

                                model.SizeId = Convert.ToInt32(objDataReader["SIZE_ID"].ToString());
                                model.SizeName = objDataReader["SIZE_NAME"].ToString();

                                if (sizeId != null)
                                {
                                    foreach (var size in sizeId)
                                    {
                                        if (!string.IsNullOrWhiteSpace(size) && objDataReader["SIZE_ID"].ToString() == size)
                                        {
                                            model.Checked = true;
                                        }
                                    }
                                }

                                objSize.Add(model);
                            }
                            return objSize;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<int>> GetProductIdByColor(int? categoryId, int? subCategoryId, int? subSubCategoryId, string[] colorId)
        {
            var sql = "SELECT DISTINCT " +
                      "PRODUCT_ID " +
                      "FROM VEW_FILTER_COLOR_AND_SIZE WHERE 1=1 ";

            if (categoryId != null && categoryId != 0)
            {
                sql += "AND CATEGORY_ID = :CATEGORY_ID ";

                if (subCategoryId != null && subCategoryId != 0)
                {
                    sql += "AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID ";

                    if (subSubCategoryId != null && subSubCategoryId != 0)
                    {
                        sql += "AND SUB_SUB_CATEGORY_ID = :SUB_SUB_CATEGORY_ID ";
                    }
                }
            }

            string colorList = string.Join(",", colorId);

                sql += "AND COLOR_ID IN (" + colorList + ")";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    if (categoryId != null && categoryId != 0)
                    {
                        objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;

                        if (subCategoryId != null && subCategoryId != 0)
                        {
                            objCommand.Parameters.Add(":SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;

                            if (subSubCategoryId != null && subSubCategoryId != 0)
                            {
                                objCommand.Parameters.Add(":SUB_SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subSubCategoryId;
                            }
                        }
                    }
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<int> objProductId = new List<int>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                var productId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());

                                objProductId.Add(productId);
                            }
                            return objProductId;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<int>> GetProductIdBySize(int? categoryId, int? subCategoryId, int? subSubCategoryId, string[] sizeId, int[] productIdByColor)
        {
            var sql = "SELECT DISTINCT " +
                      "PRODUCT_ID " +
                      "FROM VEW_FILTER_COLOR_AND_SIZE WHERE 1=1 ";

            if (categoryId != null && categoryId != 0)
            {
                sql += "AND CATEGORY_ID = :CATEGORY_ID ";

                if (subCategoryId != null && subCategoryId != 0)
                {
                    sql += "AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID ";

                    if (subSubCategoryId != null && subSubCategoryId != 0)
                    {
                        sql += "AND SUB_SUB_CATEGORY_ID = :SUB_SUB_CATEGORY_ID ";
                    }
                }
            }

            if (productIdByColor.Any())
            {
                string productByColor = string.Join(",", productIdByColor);
                sql += "AND PRODUCT_ID IN (" + productByColor + ")";
            }

            string sizeList = string.Join(",", sizeId);

            sql += "AND SIZE_ID IN (" + sizeList + ")";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    if (categoryId != null && categoryId != 0)
                    {
                        objCommand.Parameters.Add(":CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;

                        if (subCategoryId != null && subCategoryId != 0)
                        {
                            objCommand.Parameters.Add(":SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;

                            if (subSubCategoryId != null && subSubCategoryId != 0)
                            {
                                objCommand.Parameters.Add(":SUB_SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = subSubCategoryId;
                            }
                        }
                    }

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<int> objProductId = new List<int>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                var productId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());

                                objProductId.Add(productId);
                            }
                            return objProductId;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Best Selling Product

        public async Task<IEnumerable<ProductModel>> BestSellingProduct()
        {
            var sql = "SELECT " +
                      "PRODUCT_ID," +
                        "PRODUCT_NAME," +
                        "SKU," +
                        "PRIMARY_IMAGE," +
                        "SELL_AMOUNT," +
                        "TOTAL_ORDER " +
                      "FROM VEW_BEST_SELLING_PRODUCT WHERE ROWNUM <= 5 ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductModel> objBestSellingProduct = new List<ProductModel>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductModel model = new ProductModel();

                                model.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                model.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                model.Sku = objDataReader["SKU"].ToString();
                                model.PrimaryImage = objDataReader["PRIMARY_IMAGE"].ToString();
                                model.SellAmount = Convert.ToDouble(objDataReader["SELL_AMOUNT"].ToString());

                                objBestSellingProduct.Add(model);
                            }
                            return objBestSellingProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Product Review

        public async Task<string> SaveCustomerReview(CustomerReview objCustomerReview)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_PRODUCT_REVIEW_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_PRODUCT_REVIEW_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;
            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = objCustomerReview.ProductId;

            objOracleCommand.Parameters.Add("P_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCustomerReview.CustomerId) ? objCustomerReview.CustomerId : null;

            objOracleCommand.Parameters.Add("P_CUSTOMER_NAME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCustomerReview.CustomerName) ? objCustomerReview.CustomerName : null;
            objOracleCommand.Parameters.Add("P_CUSTOMER_EMAIL", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCustomerReview.CustomerEmail) ? objCustomerReview.CustomerEmail : null;
            objOracleCommand.Parameters.Add("P_PHONE_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCustomerReview.CustomerPhone) ? objCustomerReview.CustomerPhone : null;
            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.NVarchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCustomerReview.Message) ? objCustomerReview.Message : null;

            objOracleCommand.Parameters.Add("P_REVIEW_SCORE", OracleDbType.Varchar2, ParameterDirection.Input).Value = objCustomerReview.Rating;

            objOracleCommand.Parameters.Add("P_out_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_out_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<IEnumerable<CustomerReview>> GetProductReview(int productId)
        {
            const string sql = "SELECT " +
                               "PRODUCT_REVIEW_ID," +
                               "PRODUCT_ID," +
                               "CUSTOMER_ID," +
                               "CUSTOMER_NAME," +
                               "PHONE_NUMBER," +
                               "CUSTOMER_EMAIL," +
                               "MESSAGE," +
                               "REVIEW_SCORE," +
                               "REVIEW_DATE," +
                               "APPROVE_YN " +
                               "FROM VEW_PRODUCT_REVIEW WHERE PRODUCT_ID = :PRODUCT_ID AND APPROVE_YN = 'Y' ORDER BY PRODUCT_REVIEW_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<CustomerReview> objCustomerReview = new List<CustomerReview>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                CustomerReview model = new CustomerReview();

                                model.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                model.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                model.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                model.CustomerPhone = objDataReader["PHONE_NUMBER"].ToString();
                                model.CustomerEmail = objDataReader["CUSTOMER_EMAIL"].ToString();
                                model.Message = objDataReader["MESSAGE"].ToString();
                                model.Rating = Convert.ToDouble(objDataReader["REVIEW_SCORE"].ToString());
                                model.ReviewDate = objDataReader["REVIEW_DATE"].ToString();

                                objCustomerReview.Add(model);
                            }
                            return objCustomerReview;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Product Search

        public async Task<List<int>> GetSearchedProductId(string searchKey)
        {
            string sql = "SELECT DISTINCT " +
                         "PRODUCT_ID " +
                         "FROM VEW_PRODUCT_SEARCH WHERE 1=1 ";

            if (!string.IsNullOrWhiteSpace(searchKey))
            {
                if(searchKey.ToLower() == "men" || searchKey.ToLower() == "man")
                    sql += "AND ( (lower(SEARCH_CRITERIA) like lower(:SearchBy)  or upper(SEARCH_CRITERIA)like upper(:SearchBy)) AND ( (lower(SEARCH_CRITERIA) NOT like lower('%wo%')  or upper(SEARCH_CRITERIA) NOT like upper('%WO%') ) ) ) ";
                else
                    sql += "AND ( (lower(SEARCH_CRITERIA) like lower(:SearchBy)  or upper(SEARCH_CRITERIA)like upper(:SearchBy)) ) ";
            }

            sql += "ORDER BY PRODUCT_ID DESC ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    if (!string.IsNullOrWhiteSpace(searchKey))
                    {
                        objCommand.Parameters.Add(":SearchBy", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(searchKey) ?
                            $"%{searchKey}%"
                            : null;
                    }

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<int> productIdList = new List<int>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                int productId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());

                                productIdList.Add(productId);
                            }
                            return productIdList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<ProductModel>> GetSearchedProduct(string productIds)
        {
            string sql = "SELECT " +
                         "ROWNUM RN, " +
                               "PRODUCT_ID," +
                                "PRODUCT_NAME," +
                                "SKU," +
                                "PRIMARY_IMAGE," +
                                "SELL_AMOUNT," +
                                "DISCOUNT," +
                                "NEW_PRICE," +
                                "TOTALQUANTITY," +
                                "CREATE_DATE " +
                         "FROM VEW_UI_PRODUCT_INFO WHERE 1=1  ";


            if (productIds != null)
            {
                sql += "AND PRODUCT_ID IN (" + productIds + ") ORDER BY PRODUCT_ID DESC ";
            }

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductModel> objProductModel = new List<ProductModel>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductModel product = new ProductModel();

                                product.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                product.ProductName = objDataReader["PRODUCT_NAME"].ToString();

                                product.Sku = objDataReader["SKU"].ToString();

                                product.PrimaryImage = objDataReader["PRIMARY_IMAGE"].ToString();

                                product.InsertDate = objDataReader["CREATE_DATE"].ToString();

                                product.SellAmount = Convert.ToDouble(objDataReader["SELL_AMOUNT"]);
                                product.Discount = Convert.ToInt32(objDataReader["DISCOUNT"]);
                                product.NewAmount = Convert.ToDouble(objDataReader["NEW_PRICE"]);

                                product.TotalQuantity = Convert.ToDouble(objDataReader["TOTALQUANTITY"]);

                                objProductModel.Add(product);

                            }
                            return objProductModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<List<string>> GetSearchHints(string query)
        {
            query = query + "%";

            var sql = "SELECT SEARCH_NAME FROM VEW_PRODUCT_SEARCH_HINTS WHERE LOWER(SEARCH_NAME) LIKE :SEARCH_NAME ";
            sql += "AND ROWNUM <= 100 ";
            sql += "ORDER BY LENGTH(SEARCH_NAME) ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":SEARCH_NAME", OracleDbType.Varchar2, ParameterDirection.Input).Value = query.ToLower();

                    await objConnection.OpenAsync();

                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<string> grid = new List<string>();
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                string name = objDataReader["SEARCH_NAME"].ToString();
                                grid.Add(name);
                            }
                            return grid;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion
    }
}