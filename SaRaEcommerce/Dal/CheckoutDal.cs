﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using SaRaEcommerce.ViewModel;

namespace SaRaEcommerce.Dal
{
    public class CheckoutDal
    {
        private OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        public async Task<string> CheckoutCustomerSave(CheckoutModel objCheckoutModel)
        {
            string strMsg;
            string orderNumber;

            OracleCommand objOracleCommand = new OracleCommand("PRO_CUSTOMER_ORDER_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;
            objOracleCommand.Parameters.Add("P_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;

            objOracleCommand.Parameters.Add("P_REG_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.CustomerId) ? objCheckoutModel.CustomerId : null;

            objOracleCommand.Parameters.Add("P_CUSTOMER_FIRST_NAME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.FirstName) ? objCheckoutModel.FirstName : null;
            objOracleCommand.Parameters.Add("P_CUSTOMER_LAST_NAME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.LastName) ? objCheckoutModel.LastName : null;
            objOracleCommand.Parameters.Add("P_CUSTOMER_EMAIL", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.Email) ? objCheckoutModel.Email : null;
            objOracleCommand.Parameters.Add("P_CUSTOMER_PHONE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.Phone) ? objCheckoutModel.Phone : null;
            objOracleCommand.Parameters.Add("P_ADDRESS_FIRST", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.Address1) ? objCheckoutModel.Address1 : null;
            objOracleCommand.Parameters.Add("P_ADDRESS_SECOND", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.Address2) ? objCheckoutModel.Address2 : null;
            objOracleCommand.Parameters.Add("P_CITY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.City) ? objCheckoutModel.City : null;
            objOracleCommand.Parameters.Add("P_COUNTRY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.Country) ? objCheckoutModel.Country : null;
            objOracleCommand.Parameters.Add("P_SHIPPING_COST", OracleDbType.Varchar2, ParameterDirection.Input).Value = objCheckoutModel.ShippingCost;

            objOracleCommand.Parameters.Add("P_TOTAL_AMOUNT", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.TotalAmount) ? objCheckoutModel.TotalAmount : null;
            objOracleCommand.Parameters.Add("P_DISCOUNT", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.Discount) ? objCheckoutModel.Discount : null;
            objOracleCommand.Parameters.Add("P_DISCOUNT_TYPE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.DiscountType) ? objCheckoutModel.DiscountType : null;
            objOracleCommand.Parameters.Add("P_CARD_TYPE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.CardType) ? objCheckoutModel.CardType : null;
            objOracleCommand.Parameters.Add("P_CUSTOMER_IP", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.CustomerIp) ? objCheckoutModel.CustomerIp : null;
            objOracleCommand.Parameters.Add("P_TRANSACTION_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.TransId) ? objCheckoutModel.TransId : null;

            objOracleCommand.Parameters.Add("P_TRANSACTION_SUCCESS_YN", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.TransSuccessStatus) ? objCheckoutModel.TransSuccessStatus : null;

            objOracleCommand.Parameters.Add("P_ORDER_NO", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
            objOracleCommand.Parameters.Add("P_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                    orderNumber = objOracleCommand.Parameters["P_ORDER_NO"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return orderNumber;
        }

        public async Task<string> CheckoutCustomerUpdate(CheckoutModel objCheckoutModel)
        {
            string strMsg;
            string orderNumber;

            OracleCommand objOracleCommand = new OracleCommand("PRO_CUSTOMER_ORDER_UPDATE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.OrderNumber) ? objCheckoutModel.OrderNumber : null;

            objOracleCommand.Parameters.Add("P_TOTAL_AMOUNT", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.TotalAmount) ? objCheckoutModel.TotalAmount : null;
            objOracleCommand.Parameters.Add("P_CARD_TYPE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.CardType) ? objCheckoutModel.CardType : null;
            objOracleCommand.Parameters.Add("P_CUSTOMER_IP", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.CustomerIp) ? objCheckoutModel.CustomerIp : null;
            objOracleCommand.Parameters.Add("P_TRANSACTION_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.TransId) ? objCheckoutModel.TransId : null;

            objOracleCommand.Parameters.Add("P_TRANSACTION_SUCCESS_YN", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCheckoutModel.TransSuccessStatus) ? objCheckoutModel.TransSuccessStatus : null;

            objOracleCommand.Parameters.Add("P_ORDER_NO", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
            objOracleCommand.Parameters.Add("P_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                    orderNumber = objOracleCommand.Parameters["P_ORDER_NO"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return orderNumber;
        }

        public async Task<string> CheckoutProductSave(CartProduct objCartProduct)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_ORDERED_PRODUCT_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_ORDER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;

            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = objCartProduct.ProductId != 0 ? objCartProduct.ProductId.ToString() : null;

            objOracleCommand.Parameters.Add("P_PRODUCT_NAME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCartProduct.ProductName) ? objCartProduct.ProductName : null;
            objOracleCommand.Parameters.Add("P_SKU", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCartProduct.Sku) ? objCartProduct.Sku : null;

            objOracleCommand.Parameters.Add("P_COLOR_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = objCartProduct.ColorId != 0 ? objCartProduct.ColorId.ToString() : null;
            objOracleCommand.Parameters.Add("P_SIZE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = objCartProduct.SizeId != 0 ? objCartProduct.SizeId.ToString() : null;
            objOracleCommand.Parameters.Add("P_QUANTITY", OracleDbType.Varchar2, ParameterDirection.Input).Value = objCartProduct.Quantity != 0 ? objCartProduct.Quantity.ToString() : null;

            objOracleCommand.Parameters.Add("P_PRICE", OracleDbType.Varchar2, ParameterDirection.Input).Value = objCartProduct.Price > 0 ? objCartProduct.Price.ToString(CultureInfo.InvariantCulture) : null;

            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCartProduct.OrderNumber) ? objCartProduct.OrderNumber : null;


            objOracleCommand.Parameters.Add("P_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> TransactionIdUpdate(string orderNumber, string trnId)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_CUSTOMER_ORDER_TRN_UPDATE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(orderNumber) ? orderNumber : null;
            objOracleCommand.Parameters.Add("P_TRANSACTION_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(trnId) ? trnId : null;
         
            objOracleCommand.Parameters.Add("P_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> BkashPaymentSave(ExecutePaymentResponse objResponse)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_BKASH_PAYMENT_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_PAYMENT_HISTORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;

            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objResponse.OrderNumber) ? objResponse.OrderNumber : null;

            objOracleCommand.Parameters.Add("P_PAYMENT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objResponse.paymentID) ? objResponse.paymentID : null;
            objOracleCommand.Parameters.Add("P_CREATE_TIME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objResponse.createTime) ? objResponse.createTime : null;

            objOracleCommand.Parameters.Add("P_UPDATE_TIME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objResponse.updateTime) ? objResponse.updateTime : null;
            objOracleCommand.Parameters.Add("P_TRX_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objResponse.trxID) ? objResponse.trxID : null;
            objOracleCommand.Parameters.Add("P_TRANSACTION_STATUS", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objResponse.transactionStatus) ? objResponse.transactionStatus : null;

            objOracleCommand.Parameters.Add("P_AMOUNT", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objResponse.Amount) ? objResponse.Amount : null;

            objOracleCommand.Parameters.Add("P_CURRENCY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objResponse.currency) ? objResponse.currency : null;
            objOracleCommand.Parameters.Add("P_INTENT", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objResponse.intent) ? objResponse.intent : null;
            objOracleCommand.Parameters.Add("P_INVOICE_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objResponse.mercentInvoiceNumber) ? objResponse.mercentInvoiceNumber : null;


            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<CheckoutModel> GetOrderInformationByTrnId(string trnId)
        {
            const string sql = "SELECT " +
                               "ORDER_NUMBER," +
                               "CUSTOMER_FIRST_NAME," +
                               "CUSTOMER_LAST_NAME," +
                               "CUSTOMER_EMAIL," +
                               "CUSTOMER_PHONE," +
                               "CITY," +
                               "TRANSACTION_ID," +
                               "TOTAL_AMOUNT," +
                               "CARD_TYPE," +
                               "CUSTOMER_IP " +
                               "FROM CUSTOMER_ORDER WHERE TRANSACTION_ID = :TRANSACTION_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":TRANSACTION_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = trnId;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        CheckoutModel objCheckoutModel = new CheckoutModel();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objCheckoutModel.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                objCheckoutModel.FirstName = objDataReader["CUSTOMER_FIRST_NAME"].ToString();
                                objCheckoutModel.LastName = objDataReader["CUSTOMER_LAST_NAME"].ToString();
                                objCheckoutModel.Email = objDataReader["CUSTOMER_EMAIL"].ToString();
                                objCheckoutModel.Phone = objDataReader["CUSTOMER_PHONE"].ToString();
                                objCheckoutModel.City = objDataReader["CITY"].ToString();
                                objCheckoutModel.TransId = objDataReader["TRANSACTION_ID"].ToString();
                                objCheckoutModel.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();
                                objCheckoutModel.CardType = objDataReader["CARD_TYPE"].ToString();
                                objCheckoutModel.CustomerIp = objDataReader["CUSTOMER_IP"].ToString();
                            }
                            return objCheckoutModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<CheckoutModel> GetOrderInformation(string orderNumber)
        {
            const string sql = "SELECT " +
                               "ORDER_NUMBER," +
                               "ORDER_DATE," +
                               "CUSTOMER_ID," +
                               "REG_CUSTOMER_ID," +
                               "FIRST_NAME," +
                               "LAST_NAME," +
                               "EMAIL," +
                               "PHONE," +
                               "ADDRESS_FIRST," +
                               "ADDRESS_SECOND," +
                               "CITY," +
                               "COUNTRY," +
                               "SHIPPING_COST," +
                               "PAYMENT_TYPE," +
                               "PAYMENT_STATUS," +
                               "DISCOUNT_PERCENT," +
                               "TOTAL_AMOUNT " +
                               "FROM VEW_CUSTOMER_ORDER WHERE ORDER_NUMBER = :ORDER_NUMBER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        CheckoutModel objCheckoutModel = new CheckoutModel();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objCheckoutModel.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                objCheckoutModel.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                objCheckoutModel.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                objCheckoutModel.FirstName = objDataReader["FIRST_NAME"].ToString();
                                objCheckoutModel.LastName = objDataReader["LAST_NAME"].ToString();
                                objCheckoutModel.Email = objDataReader["EMAIL"].ToString();
                                objCheckoutModel.Phone = objDataReader["PHONE"].ToString();
                                objCheckoutModel.Address1 = objDataReader["ADDRESS_FIRST"].ToString();
                                objCheckoutModel.Address2 = objDataReader["ADDRESS_SECOND"].ToString();
                                objCheckoutModel.City = objDataReader["CITY"].ToString();
                                objCheckoutModel.Country = objDataReader["COUNTRY"].ToString();
                                objCheckoutModel.ShippingCost = Convert.ToInt32(objDataReader["SHIPPING_COST"].ToString());
                                
                                if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
                                    objCheckoutModel.CardType = "Cash";
                                else if(objDataReader["PAYMENT_TYPE"].ToString() == "1")
                                    objCheckoutModel.CardType = "NEXUS Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
                                    objCheckoutModel.CardType = "DBBL Master Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
                                    objCheckoutModel.CardType = "DBBL Visa Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
                                    objCheckoutModel.CardType = "Visa Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
                                    objCheckoutModel.CardType = "Master Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "6")
                                    objCheckoutModel.CardType = "Rocket Mobile Banking";
                                else
                                    objCheckoutModel.CardType = "bKash Mobile Banking";

                                objCheckoutModel.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();
                                objCheckoutModel.Discount = objDataReader["DISCOUNT_PERCENT"].ToString();
                                objCheckoutModel.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();
                            }
                            return objCheckoutModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<CartProduct>> GetOrderedProduct(string orderNumber)
        {
            const string sql = "SELECT " +
                               "ORDER_ID," +
                                "PRODUCT_ID," +
                                "PRODUCT_NAME," +
                                "SKU," +
                                "COLOR_ID," +
                                "COLOR_NAME," +
                                "SIZE_ID," +
                                "SIZE_NAME," +
                                "QUANTITY," +
                                "PRICE," +
                                "ORDER_NUMBER," +
                                "ORDER_DATE " +
                               "FROM VEW_ORDERED_PRODUCT WHERE ORDER_NUMBER = :ORDER_NUMBER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<CartProduct> objCartProducts = new List<CartProduct>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                CartProduct cartProduct = new CartProduct();

                                cartProduct.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                cartProduct.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                cartProduct.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                cartProduct.Sku = objDataReader["SKU"].ToString();
                                cartProduct.ColorName = objDataReader["COLOR_NAME"].ToString();
                                cartProduct.SizeName = objDataReader["SIZE_NAME"].ToString();
                                cartProduct.Quantity = Convert.ToInt32(objDataReader["QUANTITY"].ToString());
                                cartProduct.Price = Convert.ToDouble(objDataReader["PRICE"].ToString());

                                objCartProducts.Add(cartProduct);
                            }
                            return objCartProducts;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<DataSet> GetOrdererReport(string orderNumber)
        {
            DataSet ds = null;
            DataTable dt = new DataTable();

            const string sql = "SELECT " +
                               "ORDER_NUMBER," +
            "ORDER_DATE," +
            "ORDER_DATE_NUMBER," +
            "CUSTOMER_ID," +
            "REG_CUSTOMER_ID," +
            "CUSTOMER_NAME," +
            "CUSTOMER_EMAIL," +
            "CUSTOMER_PHONE," +
            "ADDRESS_FIRST," +
            "ADDRESS_SECOND," +
            "CITY," +
            "DELEVERY_YN," +
            "DELEVERY_DATE," +
            "RETURN_YN," +
            "RETURN_DATE," +
            "REJECT_YN," +
            "REJECT_DATE," +
            "ORDER_ID," +
            "PRODUCT_ID," +
            "PRODUCT_NAME," +
            "ITEM_NAME," +
            "DESCRIPTION," +
            "SKU," +
            "COLOR_ID," +
            "COLOR_NAME," +
            "SIZE_ID," +
            "SIZE_NAME," +
            "QUANTITY," +
            "PRICE," +
            "TOTAL_PRICE " +
                               "FROM RPT_CUSTOMER_ORDER_DETAILS WHERE ORDER_NUMBER = :ORDER_NUMBER AND DELEVERY_YN = 'N' AND RETURN_YN = 'N' AND REJECT_YN = 'N' ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;

                    using (OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand))
                    {
                        try
                        {
                            objCommand.Connection = objConnection;
                            await objConnection.OpenAsync();
                            dt.Clear();
                            ds = new System.Data.DataSet();
                            objDataAdapter.Fill(ds, "RPT_CUSTOMER_ORDER_DETAILS");

                            return ds;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataAdapter.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<double> VerifyPrivilegeCard(string cardNumber)
        {
            const string sql = "SELECT * FROM VEW_PRIVILEGE_CARD WHERE CARD_NUMBER = :CARD_NUMBER AND ACTIVE_YN = 'Y' ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":CARD_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = cardNumber;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        double discountPercentage = 0;

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                discountPercentage = Convert.ToDouble(objDataReader["DISCOUNT_PERCENTAGE"].ToString());
                            }
                            return discountPercentage;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #region Validate Product And Price

        public int GetInventoryValue(int productId, int colorId, int sizeId)
        {
            int quantity = 0;

            string sql = "select NVL(sum(QUANTITY), 0) total_inventory from VEW_PRODUCT_INVENTORY where PRODUCT_ID = :PRODUCT_ID AND COLOR_ID = :COLOR_ID AND SIZE_ID = :SIZE_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
                    objCommand.Parameters.Add(":COLOR_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = colorId;
                    objCommand.Parameters.Add(":SIZE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = sizeId;

                    objConnection.Open();
                    using (OracleDataReader objDataReader = objCommand.ExecuteReader())
                    {
                        try
                        {
                            while (objDataReader.Read())
                            {
                                quantity = quantity + Convert.ToInt32(objDataReader["total_inventory"]);
                            }
                            return quantity;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public double GetProductPrice(int productId, int colorId, int sizeId)
        {
            double price = 0;

            string sql = "select PRICE from VEW_PRODUCT_INVENTORY where PRODUCT_ID = :PRODUCT_ID AND COLOR_ID = :COLOR_ID AND SIZE_ID = :SIZE_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
                    objCommand.Parameters.Add(":COLOR_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = colorId;
                    objCommand.Parameters.Add(":SIZE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = sizeId;

                    objConnection.Open();
                    using (OracleDataReader objDataReader = objCommand.ExecuteReader())
                    {
                        try
                        {
                            while (objDataReader.Read())
                            {
                                price = Convert.ToDouble(objDataReader["PRICE"].ToString());
                            }
                            return (int) price;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

    }
}